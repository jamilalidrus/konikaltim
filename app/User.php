<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable;
  protected $table='users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','username','email','password','gambar','tgl_lahir','kelamin','ayah','ibu','no_hp','berat_badan','olahraga_id','status_atlit','agama','status','tinggi','darah','provinsi_id','kabupaten_id','kota_id','kecamatan_id','kelurahan_id','sekolah','kelas','alamat_sekolah','sepatu','kaos','kameja','hobi','kota_domisili'];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'password', 'remember_token',
    ];


    public function prestasi(){
      return $this->hasMany('App\Model\Prestasi','user_id','id');
    }



    public function provinsi(){
      return $this->belongsTo(Model\Provinsi::class);
    }
    public function kabupaten(){
      return $this->belongsTo(Model\Kabupaten::class);
    }
    public function kota(){
      return $this->belongsTo(Model\Kota::class);
    }
    public function kecamatan(){
      return $this->belongsTo(Model\Kecamatan::class);
    }
    public function kelurahan(){
      return $this->belongsTo(Model\Kelurahan::class);
    }

    public function sertifikat(){
      return $this->hasOne(Model\Sertifikat::class,'user_id','id');
    }

    public function notif(){
      return $this->hasMany(Model\Notif::class,'user_id','id');
    }

    public function atlitpertandingan(){
      return $this->hasMany(Model\AtlitPertandingan::class);
    }

    public function cabor(){
      return $this->belongsTo(Model\Cabor::class,'olahraga_id','id');
    }

    public function medali(){
      return $this->belongsTo(Model\Medali::class);
    }

    public function domisili(){
      return $this->belongsTo('App\Model\KotaDomisili','kota_domisili','id');
    }

    public function scopeFilter($query,$request){
      if ($request->has('name')) {
         $query->where('name','LIKE','%'.$request->get('name').'%');
      }
      if ($request->has('cabor')) {
        $query->where('olahraga_id',$request->get('cabor'));
      }
    }

  }
