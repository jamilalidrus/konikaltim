<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CaborRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_cabor'=>'required|unique:cabor',
        ];
    }

    public function messages()
    {
        return [
            'nama_cabor.required'=>'Bagian ini tidak boleh kosong',
            'nama_cabor.unique'=>'Cabor ini sudah ada',
        ];
    }
}
