<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PendaftaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name'=>'required',
            'email'=>'required|email',
            'password'=>'required',
            'repassword'=>'required|same:password',
            'gambar'=>'required|mimes:JPG,JPEG,PNG,jpg,jpeg,png|max:2000',
            'tgl_lahir'=>'required|date|before:today',
            'kelamin'=>'required',
            'ayah'=>'required',
            'ibu'=>'required',
            'no_hp'=>'required',
            'berat_badan'=>'required',
            'olahraga_id'=>'required',
            'status_atlit'=>'required',
            'agama'=>'required',           
            'tinggi'=>'required',
            'darah'=>'required',
            'provinsi_id'=>'required',
            'kabupaten_id'=>'required',
            'kota_id'=>'required',
            'kecamatan_id'=>'required',
            'kelurahan_id'=>'required',
            'sekolah'=>'required',
            'kelas'=>'required',
            'alamat_sekolah'=>'required',
            'sepatu'=>'required',
            'kaos'=>'required',
            'kameja'=>'required',
            'hobi'=>'required',
            'nama_sertifikat'=>'mimes:zip,rar'
        ];
    }
    public function messages()
    {
        return [
          'name.required'=>'bagian nama wajib diisi',
            'email.required'=>'bagian email wajib diisi',
            'email.email'=>'bagian email harus berisi email',
            'password.required'=>'bagian password harus diisi',
            'repassword.same'=>'password tidak sama',
            'repassword.required'=>'masukkan ulang password',
            'gambar.required'=>'bagian gambar harus diisi',
            'gambar.mimes'=>'gambar setidaknya memiliki format JPG,PNG atau JPEG',
            'tgl_lahir.required'=>'bagian tanggal wajib diisi',
            'tgl_lahir.date'=>'bagian tanggal tidak valid',
            'kelamin.required'=>'jenis kelamin wajib dipilih',
            'ayah.required'=>'nama ayah wajib diisi',
            'ibu.required'=>'nama ibu wajib diisi',
            'no_hp.required'=>'no ponsel wajib diisi',
            'berat_badan.required'=>'berat badan wajib diisi',
            'olahraga_id.required'=>'bidang olahraga wajib diisi',
            'status_atlit.required'=>'status atlit wajib di isi',
            'agama.required'=>'bagian agama wjib diisi',           
            'tinggi.required'=>'tinggi badan wajib diisi',
            'darah.required'=>'golongan darah wajib diisi',
            'provinsi_id.required'=>'bagian provinsi wajib diisi',
            'kabupaten_id.required'=>'bagian kabupaten wajib diisi',
            'kota_id.required'=>'nama kota wajib diisi',
            'kecamatan_id.required'=>'bagian kecamatan wajib diisi',
            'kelurahan_id.required'=>'bagian keluarahan wajib diisi',
            'sekolah.required'=>'nama sekolah wajib diisi',
            'kelas.required'=>'tingkatan kelas wajib diisi',
            'alamat_sekolah.required'=>'alamat sekolah wajib diisi',
            'sepatu.required'=>'ukuran sepatu wajib diisi',
            'kaos.required'=>'ukuran kaos wajib diisi',
            'kameja.required'=>'ukuran kameja wajib diisi',
            'hobi.required'=>'hobi wajib diisi',
            'nama_sertifikat.mimes'=>'file setidaknya berformat zip atau rar',
        ];
    }
}
