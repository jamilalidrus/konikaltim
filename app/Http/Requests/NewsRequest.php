<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'judul'=>'required|unique:news',
            'gambar'=>'required|mimes:JPG,JPEG,PNG,jpg,jpeg,png',
            'isi'=>'required|min:10',
        ];
    }

    public function messages()
    {
        return [
            'judul.required'=>'bagian judul tidak boleh kosong',
            'judul.unique'=>'berita dengan judul ini sudah ada',
            'gambar.required'=>'gambar tidak boleh kosong',
            'gambar.mimes'=>'format gambar tidak didukung',
            'isi.required'=>'bagian isi tidak boleh kosong',
            'isi.min'=>'isi berita setidaknya harus lebih dari 10 karakter',
        ];
    }
}
