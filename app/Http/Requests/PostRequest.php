<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'saran'=>'required|min:10',
            'gambar'=>'mimes:JPG,JPEG,PNG,jpg,jpeg,png',
        ];
    }

    public function messages(){
        return [

            'saran.required'=>'bagian text ini tidak boleh kosong',
            'saran.min'=>'kiriman setidaknya harus berisi minimal 10 karakter',
            'gambar.mimes'=>'format gambar tidak di dukung',

        ];
        
    }
}
