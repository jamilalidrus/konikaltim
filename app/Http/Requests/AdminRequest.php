<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|unique:admins',
            'password'=>'required',
            'password_confirm'=>'required|same:password',
            'gambar'=>'required|mimes:JPG,JPEG,PNG,jpg,jpeg,png',
            'name'=>'required',
           
        ];
    }

    public function messages()
    {
        return [
            'email.required'=>'email harus diisi',
            'password.required'=>'password harus diisi',
            'password_confirm.required'=>'konfirmasi password',
            'password_confirm.same'=>'password tidak sama',
            'gambar.required'=>'gambar harus diisi',
            'gambar.mimes'=>'format gambar tidak berlaku',
            'name.required'=>'bagian nama harus di isi',
            'email.unique'=>'email sudah ada',
           
        ];
    }

}
