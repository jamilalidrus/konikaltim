<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cabor;

class CaborController extends Controller
{
    public function __construct(Cabor $cabor){
    	$this->middleware('auth:web');
    	$this->cabor=$cabor;
    }

    public function index(){
    	$cabor=$this->cabor->orderBy('id','desc')->paginate(20);
    	return view('User.cabor.index',compact('cabor'));
    }
    public function create(){
    	return abort(404);
    }
    public function show(){
    	return abort(404);
    }
     public function edit(){
    	return abort(404);
    }
}
