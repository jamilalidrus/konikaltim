<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class AtlitController extends Controller
{
    public function __construct(User $user){
    	$this->user=$user;
    	$this->middleware('auth:web');
    }
    public function index(){
    	$user=$this->user->where('status','accepted')->orderBy('id','desc')->paginate(20);
    	return view('User.atlit.index',compact('user'));
    }
}
