<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Notif;
use Auth;
class NotifController extends Controller
{
    public function __construct(Notif $notif){
    	$this->notif=$notif;
    	$this->middleware('auth:web');
    }

    public function index(){
    	$notif=Notif::where('user_id',Auth::user()->id)->orderBy('id','desc')->paginate(30);
    	return view('User.notif.listnotif',compact('notif'));
    }

    public function show($id){
    	$notif=$this->notif->findOrFail($id);
    	if (Auth::user()->id == $notif->user_id) {
    		$notif= Notif::find($id);
    		$notif['status']='terbaca';
    		$notif->update();
    		return view('User.notif.readnotif',compact('notif'));
    	}else{
    		return abort(404);
    	}
    	
    }

}
