<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth,Session,Hash,File;
use App\Model\Provinsi;
use App\Model\KotaDomisili;
class ProfileSelfController extends Controller
{

    public function __construct(User $user,Provinsi $provinsi,KotaDomisili $domisili){
    	$this->middleware('auth:web');
        $this->user=$user;
        $this->provinsi=$provinsi;
    	$this->domisili=$domisili;
    }
    public function index(){
    	$self=Auth::user();
    	$user=$this->user->findOrFail($self->id);
    	return view('User.profil.profileself',compact('user'));
    }
    public function edit($id){
    	$self=Auth::user();
    	if ($id== $self->id ) {
            $provinsi=$this->provinsi->all()->pluck('nama_provinsi','id');
           $kabkota=Provinsi::find(4)->kabupaten->pluck('nama_kabupaten','id');
    		$user=$this->user->findOrFail($id);
    		return view('User.profil.edit',compact('user','provinsi','kabkota'));
    	}else{
    		return abort(404);
    	}
    	
    }
    public function update($id,Request $request){
    	$this->validate($request,[
    		'name'=>'required',
            'email'=>'required|email',
           
            'repassword'=>'same:password',
            'gambar'=>'mimes:JPG,JPEG,PNG,jpg,jpeg,png',
            'tgl_lahir'=>'required|date|before:today',
            'kelamin'=>'required',
            'ayah'=>'required',
            'ibu'=>'required',
            'no_hp'=>'required',
            'berat_badan'=>'required',
          
            'status_atlit'=>'required',
            'agama'=>'required',           
            'tinggi'=>'required',
            'darah'=>'required',
           
            'sekolah'=>'required',
            'kelas'=>'required',
            'alamat_sekolah'=>'required',
            'sepatu'=>'required',
            'kaos'=>'required',
            'kameja'=>'required',
            'hobi'=>'required',
          
    		]);

    	$user=$request->all();
		$user['username']=$this->user->find($id)->username;
		if ($request->hasFile('gambar')) {
			$file=$request->file('gambar');
			$direktori=public_path().'/gambar/user/';
			File::delete($direktori.$this->user->find($id)->gambar);			
			$nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
			$uploadSuccess=$file->move($direktori,$nama_file);
			$user['gambar']=$nama_file;	
		}else{
			$user['gambar']=$this->user->find($id)->gambar;
		}

		if ($request->has('password')) {
			$user['password']=Hash::make($request->password);
		}else{
			$user['password']=$this->user->find($id)->password;
		}

    	$this->user->findOrFail($id)->update($user);
    	Session::flash('message','Data berhasil diubah');
    	return redirect('atlit/profile');

    }
}
