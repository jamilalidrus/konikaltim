<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
class UserloginController extends Controller
{
    public function FormLogin(){
    	return view('login.loginuser');
    }
    public function proses(Request $request){

    	$this->validate($request,[
    		'email'=>'required|email',
    		'password'=>'required|min:6',
    		]);

    	if (Auth::guard('web')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember))
    	{
    		return redirect('atlit/home');
    	}else{
            Session::flash('message','email dan password anda salah');
        return redirect()->back()->withInput($request->only('email','remember'));
        }

        
    }

    public function logout(){
        Auth::guard('web')->logout();
         Session::flush();
        return redirect('atlit/page-login');
    }
}
