<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Session;
class UsersingupController extends Controller
{
	public function __construct(User $user){
		$this->user=$user;
	}
    public function index(){
    	return view('User.register.register');
    }
    public function register(Request $request){
    	$this->validate($request,[
    		'name'=>'required|unique:users',
    		'email'=>'required|email|unique:users',
    		'password'=>'required|min:6',
    		'passwordconfirm'=>'required|same:password',
    		'tgl_lahir'=>'required',
    		'alamat'=>'required',
    		'picture'=>'required',
            'kelurahan'=>'required',
            'rt'=>'required',
            'rw'=>'required',
            'no_rumah'=>'required',
            'jalan'=>'required',
    		]);

    	$user=$request->all();
    	$user['password']=Hash::make($request->password);
    	$username=strtolower($request->name);
    	$user['username']=str_replace(' ','', $username);
		if ($request->hasFile('picture')) {
			$file=$request->file('picture');
			$direktori=public_path().'/gambar/user/';			
			$nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
			$uploadSuccess=$file->move($direktori,$nama_file);
			$user['picture']=$nama_file;	
		}
			
		$this->user->create($user);
		Session::flash('message','akun anda berhasil dibuat');
		return redirect()->back();
    }
}
