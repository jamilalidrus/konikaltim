<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,Session;

class AdminLoginController extends Controller
{
	public function __construct(){

	}
    public function showLoginForm(){
    	return view('login.loginadmin');
    }
    public function login(Request $request){
    	$this->validate($request,[
    		'email'=>'required|email',
    		'password'=>'required|min:6',
    		]);
    	if (Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember))
    	{
    		return redirect()->intended('admin/homepage');
    	}
        Session::flash('message','email dan password salah');
    	return redirect()->back()->withInput($request->only('email','remember'));
    }

    public function logout(){        
        Auth::guard('admin')->logout();
        Session::flush();
        return redirect('admin/login');
    }

    
}
