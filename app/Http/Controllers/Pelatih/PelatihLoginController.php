<?php

namespace App\Http\Controllers\Pelatih;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Pelatih;
use Session;
class PelatihLoginController extends Controller
{

	
	public function form(){
		return view('login.loginpelatih');
	}

	public function proses(Request $request){
		if (Auth::guard('pelatih')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember)){
				return redirect('pelatih/landing');
		}else{
			Session::flash('message','email dan password salah');
			return redirect()->back()->withInput($request->all());
		}
	}

	public function tespelatih(){
		if (Auth::check()) {
			return 'udah';
		}else{
			return 'belum';
		}
	}

	public function logout(){
		Auth::guard('pelatih')->logout();
		 Session::flush();
		return redirect('pelatih/login');

	}
}
