<?php

namespace App\Http\Controllers\Pelatih;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Pengumuman;
use App\Model\Notif;
use App\User;
use Auth,Session;

class PengumumanController extends Controller
{
	public function __construct(){
		$this->middleware('auth:pelatih');
	}
    public function index(){
    	$pengumuman= Pengumuman::where('pelatih_id',Auth::user()->id)->orderBy('id','desc')->paginate(20);
    	return view('Pelatih.pengumuman.index',compact('pengumuman'));
    }
    public function create(){

    	return view('Pelatih.pengumuman.tambah');
    }
    public function store(Request $request){
    	$this->validate($request,[
    		'judul'=>'required',
    		'isi'=>'required',
    		]);
    	$user= User::where('olahraga_id',Auth::user()->pelatih_id)->get();

    	$collection=$user->pluck('id');
    	
    	$pengumuman= new Pengumuman;
    	$pengumuman->judul=$request->judul;
    	$pengumuman->pelatih_id=Auth::user()->id; 
    	$pengumuman->cabor_id=Auth::user()->pelatih_id;
    	$pengumuman->isi=$request->isi; 
    	$pengumuman->tujuan= json_encode($collection) ;
        
    	$pengumuman->save();
    	foreach ($user as $key) {
    	$notif= new Notif;
    	$notif->isi='pengumuman terbaru telah tersedia, silahkan cek halaman pengumuman anda!';
    	$notif->user_id=$key->id;
    	$notif->pelatih_id=Auth::user()->id;
    	$notif->save();
    	}    	

    	Session::flash('message','pengumuman berhasil disebarkan kepada atlit anda');
    	return redirect('pelatih/pengumuman');
    	
    }
     public function edit($id){
    	$pengumuman=Pengumuman::findOrFail($id);
    	if ($pengumuman->pelatih_id == Auth::user()->id) {
    		return view('Pelatih.pengumuman.ubah',compact('pengumuman'));
    	}else{
    		return abort('404');
    	}
    	
    }
     public function update(Request $request,$id){
    	$this->validate($request,[
    		'judul'=>'required',
    		'isi'=>'required',
    		]);
    	$user= User::where('olahraga_id',Auth::user()->pelatih_id)->get();
    	$collection=$user->pluck('id')->toArray();
    	
    	$pengumuman= Pengumuman::find($id);
    	$pengumuman->judul=$request->judul;
    	$pengumuman->pelatih_id=Auth::user()->id; 
    	$pengumuman->cabor_id=Auth::user()->pelatih_id;
    	$pengumuman->isi=$request->isi; 
    	$pengumuman->tujuan= json_encode($collection) ;
    	$pengumuman->update();
    	Session::flash('message','pengumuman berhasil diubah');
    	return redirect('pelatih/pengumuman');
    }
     public function destroy($id){
     	$pengumuman=Pengumuman::findOrFail($id);
     	if ( $pengumuman->pelatih_id == Auth::user()->id) {
     		$pengumuman->destroy($id);
    		Session::flash('message','pengumuman berhasil dihapus');
    		return redirect('pelatih/pengumuman');
     	}else{
     		return abort(404);
     	}
    	
    }



}
