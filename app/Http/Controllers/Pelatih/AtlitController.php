<?php

namespace App\Http\Controllers\Pelatih;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;	
use Auth;
class AtlitController extends Controller
{
	public function __construct(User $atlit){
		$this->atlit=$atlit;
	}
    public function mine(){
    	$atlit=$this->atlit->where('olahraga_id',Auth::user()->pelatih_id)->where('status','accepted')->orderBy('id','desc')->paginate(20);
    	return view('Pelatih.AtlitBinaan.index',compact('atlit'));
    }

    public function all(){
    	$atlit=$this->atlit->where('status','accepted')->orderBy('id','desc')->paginate(20);
    	return view('Pelatih.Atlit.index',compact('atlit'));
    }
}
