<?php

namespace App\Http\Controllers\Pelatih;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Pertandingan;
use App\Model\AtlitPertandingan;
use App\Model\Cabor;
use App\User;
use Auth,Session;
class SelectAtlitController extends Controller
{
	public function __construct(Pertandingan $pertandingan, Cabor $cabor, User $atlit){
		$this->pertandingan=$pertandingan;
		$this->cabor=$cabor;
		$this->atlit=$atlit;
	}

	public function index(){

		$atlitpertandingan=AtlitPertandingan::where('pelatih_id',Auth::user()->id)->get();
		return view('Pelatih.Addlomba.index',compact('atlitpertandingan'));
	}

   public function pertandingan($id){
   	 
   		$cabor=$this->cabor->where('status','active')->pluck('nama_cabor','id');
    	$pertandingan=$this->pertandingan->findOrFail($id);
    	$atlit=$this->atlit->where('olahraga_id',Auth::user()->pelatih_id)->where('status','accepted')->get();
    	return view('Pelatih.Addlomba.tambah',compact('cabor','pertandingan','atlit'));    	

   }

   public function store($id,Request $request){
   		

   		foreach ($request->user_id as $calon) {
   			$atlitpertandingan=new AtlitPertandingan;
   			$atlitpertandingan->user_id=$calon;	
   			$atlitpertandingan->pertandingan_id=$id;	
   			$atlitpertandingan->pelatih_id=Auth::user()->id;
   			$atlitpertandingan->cabor_id=Auth::user()->pelatih_id;
   			$atlitpertandingan->provinsi_id=Auth::user()->provinsi_id;
   			$atlitpertandingan->save();
   		}
   		Session::flash('message','atlit telah terpilih');
   		return redirect('pelatih/atlitpertandingan');
   }

   public function destroy($id){
   		AtlitPertandingan::destroy($id);
   		Session::flash('message','atlit pilihan berhasil dibatalkan');
   		return redirect('pelatih/atlitpertandingan');
   }
}
