<?php

namespace App\Http\Controllers\Pelatih;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Session;
use App\Model\Notif;
use Auth;
class AcceptAtlitController extends Controller
{
    public function __construct(User $atlit){
    	$this->atlit=$atlit;
    }

    public function index(){
    	$atlit=$this->atlit->where(['status'=>'waiting'])->orderBy('id','desc')->paginate(20); 
      
    	return view('Pelatih.Calonatlit.index',compact('atlit'));
    }

    public function show($id){
    	$atlit=$this->atlit->find($id);
    	return view('Pelatih.Calonatlit.showatlit',compact('atlit'));
    }
    public function download($id){
    	$atlit=$this->atlit->findOrFail($id);    	    	
    	if (empty($atlit->sertifikat->nama_sertifikat)){
    		return response(404);
    	}elseif(!empty($atlit->sertifikat->nama_sertifikat)){
    		return response()->download(public_path().'/file/sertifikat/'.$atlit->sertifikat->nama_sertifikat);
    	}
    }

    public function accept($id){
    	$atlit= User::findOrFail($id);
    	$atlit->status='accepted';
    	$atlit->update();

    	$notif= new Notif;
    	$notif->isi='Selamat anda berhasil diterima sebagai atlit '.$atlit->cabor->nama_cabor.' di koni kaltim silahkan hubungi pelatih anda '.Auth::user()->nama.'  '.Auth::user()->email.' untuk mendapatkan update informasi terbaru, pemeritahuan lain dapat anda lihat pada menu pengumuman';
    	$notif->user_id=$atlit->id;
    	$notif->pelatih_id=Auth::user()->id;
    	$notif->status='belum';
    	$notif->save();
    	Session::flash('message','atlit telah anda terima,');
    	return redirect()->back();
    	
    }

    public function reject($id){
        $atlit= User::findOrFail($id);
        $atlit->status='rejected';
        $atlit->update();

        $notif= new Notif;
        $notif->isi='Mohon maaf anda belum diterima sebagai atlit '.$atlit->cabor->nama_cabor.' di koni kaltim, silahkan mencoba dilain waktu';
        $notif->user_id=$atlit->id;
        $notif->pelatih_id=Auth::user()->id;
        $notif->status='belum';
        $notif->save();
        Session::flash('message','atlit telah anda tolak,');
        return redirect()->back();
    }
}
