<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Provinsi;
use App\Model\Kabupaten;
use App\Model\Kota;
use App\Model\Kecamatan;
use App\Model\Kelurahan;
class DropdownController extends Controller
{


    public function dropdown(Request $request){
		switch ($request->type):			
			case 'kabupaten_id':
				$return = '<option value="">Select Options</option>';
				foreach(Kabupaten::where('provinsi_id', $request->id)->get() as $row) 
					$return .= "<option value='$row->id'>$row->nama_kabupaten</option>";
				return $return;
				break;
			case 'kota_id':
				$return='<option value="">Select Options</option>';
				foreach (Kota::where('kabupaten_id',$request->id)->get() as $row)
					$return.="<option value='$row->id'>$row->nama_kota</option>";	
					return $return;			
				break;
			case 'kecamatan_id':
				$return='<option value="">Select Options</option>';
				foreach (Kecamatan::where('kota_id',$request->id)->get() as $row)
					$return.="<option value='$row->id'>$row->nama_kecamatan</option>";
					return $return;				
				break;
			case 'kelurahan_id':
				$return='<option value="">Select Options</option>';
				foreach (Kelurahan::where('kecamatan_id',$request->id)->get() as $row)
					$return.="<option value='$row->id'>$row->nama_kelurahan</option>";
					return $return;				
				break;
			
			
			endswitch;
		
	}
}
