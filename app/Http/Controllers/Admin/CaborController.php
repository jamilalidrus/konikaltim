<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cabor;
use App\Http\Requests\CaborRequest;
use Session;
use Yajra\Datatables\Datatables;
use DB;

class CaborController extends Controller
{
    public function __construct(Cabor $cabor){
    	$this->cabor=$cabor;
        $this->middleware('auth:admin');
    }

    public function index(){
        // $cabor=$this->cabor->orderBy('id','desc')->paginate(20);
        return view('Admin.cabor.index',compact('cabor'));
    }
    public function create(){
        return view('Admin.cabor.tambah');
    }
    public function store(CaborRequest $request){
        $cabor=$request->all();
        $this->cabor->create($cabor);
        Session::flash('message','data berhasil ditambah');
        return redirect('admin/cabor');
    }
    public function edit($id){
        $cabor=$this->cabor->findOrFail($id);
        return view('Admin.cabor.ubah',compact('cabor'));
    }
    public function update($id,Request $request){
        $this->validate($request,[
            'nama_cabor'=>'required',
            ]);
        $cabor=$request->all();
        $this->cabor->findOrFail($id)->update($cabor);
        Session::flash('message','data berhasil diubah');
        return redirect('admin/cabor');
    }

    // public function show($id){
    //     return $id;
    // }
    public function destroy($id){
    	$this->cabor->destroy($id);
        Session::flash('message','data berhasil dihapus');
        return redirect()->back();
    }

    public function anyData(){
        DB::statement(DB::raw('set @rownum=0'));
        $cabor=Cabor::all(DB::raw('@rownum := @rownum + 1 AS number '),'nama_cabor','id');
        return Datatables::of($cabor)
         ->addColumn('action', function ($cabor) {
                return '
                <form method="POST" action="'.route('cabor.destroy',$cabor->id).'" onsubmit="return confirm()">
                    <a href="cabor/'.$cabor->id.'/edit" class="btn  btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <button type="submit" value="DELETE!" class="btn btn-danger" >Hapus</button>
                </form>
                ';
              
        })
        ->make(true);
    }
}
