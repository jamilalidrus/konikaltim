<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cabor;
use App\Model\Regu;
use App\Model\Kota;
use Session;
class ReguController extends Controller
{

    public function __construct(Cabor $cabor,Regu $regu, Kota $kota){
        $this->cabor=$cabor;
        $this->regu=$regu;
        $this->kota=$kota;
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regu=$this->regu->orderBy('id','desc')->paginate(20);
        return view('Admin.regu.index',compact('regu'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cabor=$this->cabor->pluck('nama_cabor','id');
        $kota=$this->kota->pluck('nama_kota','id');
        return view('Admin.regu.tambah',compact('cabor','kota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'cabor_id'=>'required',
            'kota_id'=>'required',
        ]);

        $regu=$request->all();
        $this->regu->create($regu);
        Session::flash('message','data regu berhasil ditambahkan');
        return redirect('admin/regu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cabor=$this->cabor->pluck('nama_cabor','id');
        $kota=$this->kota->pluck('nama_kota','id');
        $regu=$this->regu->findOrFail($id);

        return view('Admin.regu.ubah',compact('cabor','kota','regu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'cabor_id'=>'required',
            'kota_id'=>'required',
        ]);

        $regu=$request->all();
        $this->regu->findOrFail($id)->update($regu);
        Session::flash('message','data regu berhasil diubah');
        return redirect('admin/regu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->regu->destroy($id);
        Session::flash('message','data regu berhasil dihapus');
        return redirect('admin/regu');
    }
}
