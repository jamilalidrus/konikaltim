<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Prestasi;
use App\Model\Cabor;
use App\User;
use App\Model\Medali;
use Session;
class PrestasiController extends Controller
{
    public function __construct(Prestasi $prestasi,Cabor $cabor,User $user,Medali $medali){
        $this->prestasi=$prestasi;    
        $this->cabor=$cabor;    
        $this->user=$user;    
        $this->medali=$medali;    
        $this->middleware('auth:admin');
    }

    public function index(Request $request){
        $prestasi=$this->prestasi->orderBy('id','desc')->filter($request)->paginate(20);
        $cabor = $this->cabor->all()->pluck('nama_cabor','id');
        $medali =$this->medali->all()->pluck('medali','id');
        return view('Admin.prestasi.index',compact('prestasi','cabor','medali'));
    }
    public function create(){
        $cabor=$this->cabor->all()->pluck('nama_cabor','id');
        $medali=$this->medali->pluck('medali','id');
        $user=$this->user->where('status','accepted')->pluck('name','id');
        return view('Admin.prestasi.tambah',compact('cabor','user','medali'));
    }
    public function store(Request $request){
        $this->validate($request,[
            'user_id'=>'required',
            'piala_id'=>'required',
            'cabor_id'=>'required',
            ]);
        $prestasi=$request->all();
        $prestasi['jenis_piala']='Emas';
        $data=$this->prestasi->create($prestasi);
        Session::flash('message','data prestasi "'.$data->user->name.'" berhasil ditambahkan');
        return redirect('admin/prestasi');
    }
    public function edit($id){
        $prestasi=$this->prestasi->findOrFail($id);
        $cabor=$this->cabor->all()->pluck('nama_cabor','id');
        $medali=$this->medali->pluck('medali','id');
        $user=$this->user->where('status','accepted')->pluck('name','id');
        return view('Admin.prestasi.ubah',compact('prestasi','cabor','user','medali'));
    }
    public function update($id,Request $request){
        $this->validate($request,[
            'user_id'=>'required',
            'piala_id'=>'required',
            'cabor_id'=>'required',
            ]);
        $prestasi=$request->all();
        $prestasi['jenis_piala']='Emas';
        $this->prestasi->findOrFail($id)->update($prestasi);
        Session::flash('message','data "'.$this->prestasi->findOrFail($id)->user->name.'" berhasil diubah');
        return redirect('admin/prestasi');
    }
    public function show(){

    }
    public function destroy($id){
    	$this->prestasi->destroy($id);
        Session::flash('message','data berhasil dihapus');
        return redirect()->back();
    }
}
