<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Medali;
use App\User;
use Session;
class MedaliController extends Controller
{
    public function __construct(Medali $medali){
        $this->medali=$medali;
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medali=$this->medali->all();
        return view('Admin.medali.index',compact('medali'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {       

        return view('Admin.medali.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'medali'=>'required',
            ]);
        $medali=$request->all();
        $this->medali->create($medali);
        Session::flash('message','data berhasil disimpan');
        return redirect('admin/medali');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $medali=$this->medali->findOrFail($id);
        return view('Admin.medali.ubah',compact('medali'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $this->validate($request,[
            'medali'=>'required',
            ]);
        $medali=$request->all();
        $this->medali->findOrFail($id)->update($medali);
        Session::flash('message','data berhasil diubah');
        return redirect('admin/medali');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->medali->destroy($id);
         Session::flash('message','data berhasil dihapus');
        return redirect('admin/medali');
    }
}
