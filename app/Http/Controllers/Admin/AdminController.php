<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Http\Requests\AdminRequest;
use Session,File,Hash;
class AdminController extends Controller
{
    public function __construct(Admin $admin){
        $this->middleware('auth:admin');        
        $this->admin=$admin;
    }
    public function index(){
        $admin=$this->admin->orderBy('id','desc')->paginate(10);
    	return view('Admin.admin.index',compact('admin'));
    }
    public function create(){
        return view('Admin.admin.tambah');
    }
    public function store(AdminRequest $request){

    	$admin=$request->all();
        $admin['password']=Hash::make($request->password);
        if ($request->hasFile('gambar')) {
            $file=$request->file('gambar');
            $direktori=public_path().'/gambar/admin/';           
            $nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
            $uploadSuccess=$file->move($direktori,$nama_file);
            $admin['gambar']=$nama_file;    
        }

        $this->admin->create($admin);
        Session::flash('message','data berhasil ditambah');
        return redirect('admin/admin');

    }
    public function edit($id){
    	$admin=$this->admin->findOrFail($id);
        return view('Admin.admin.ubah',compact('admin'));
    }
    public function update($id,Request $request){
        $this->validate($request,[
            'name'=>'required',
            'password_confirm'=>'same:password',
            'gambar'=>'mimes:JPG,JPEG,PNG,jpg,jpeg,png',
            'email'=>'required',
            ]);
    	$admin=$request->all();
       
        if ($request->hasFile('gambar')) {
            $file=$request->file('gambar');
            $direktori=public_path().'/gambar/admin/'; 
            File::delete($direktori.$this->admin->find($id)->gambar);          
            $nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
            $uploadSuccess=$file->move($direktori,$nama_file);
            $admin['gambar']=$nama_file;    
        }else{
            $admin['gambar']=$this->admin->find($id)->gambar;
        }
        if ($request->has('password')) {
             $admin['password']=Hash::make($request->password);
        }else{
            $admin['password']=$this->admin->find($id)->password;
        }

        $this->admin->find($id)->update($admin);
        Session::flash('message','data berhasil diubah');
        return redirect('admin/admin');

    }
    public function destroy($id){
         $direktori=public_path().'/gambar/admin/';
         File::delete($direktori.$this->admin->find($id)->gambar);
         $this->admin->destroy($id);
         Session::flash('message','data berhasil dihapus');
         return redirect('admin/admin');

    }
}
