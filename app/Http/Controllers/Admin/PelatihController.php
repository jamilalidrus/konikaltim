<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pelatih;
use App\Model\Provinsi;
use App\Model\Cabor;
use Hash,File,Session;

class PelatihController extends Controller
{
    public function __construct(Pelatih $pelatih,Provinsi $provinsi,Cabor $cabor){
    	$this->pelatih=$pelatih;
    	$this->provinsi=$provinsi;
    	$this->cabor=$cabor;
    	$this->middleware('auth:admin');
    }
    public function index(Request $request){
        $pelatih=$this->pelatih->orderBy('id','desc')->filter($request)->paginate(30);
        $cabor=$this->cabor->all()->pluck('nama_cabor','id');
        return view('Admin.pelatih.index',compact('pelatih','cabor'));
    }

    public function create(){    	
    	$provinsi=$this->provinsi->all()->pluck('nama_provinsi','id');
    	$cabor=$this->cabor->all()->pluck('nama_cabor','id');
    	return view('Admin.pelatih.tambah',compact('provinsi','cabor'));
    }
    public function store(Request $request){
    	$this->validate($request,[
    		'nama'=>'required',
    		'email'=>'required|email',
    		'password'=>'required|min:6|max:20',
    		'gambar'=>'required|mimes:JPG,JPEG,PNG,jpg,jpeg,png',
    		'alamat_rinci'=>'required',
    		'tgl_lahir'=>'required|date|before:today',
    		'pelatih_id'=>'required',
    		'kelamin'=>'required',
    		'provinsi_id'=>'required',
    		'kabupaten_id'=>'required',
    		'kota_id'=>'required',
    		'kecamatan_id'=>'required',
    		'kelurahan_id'=>'required',
    		'no_hp'=>'required',
    		'agama'=>'required',
    		]);
    	$pelatih=$request->all();
    	$pelatih['password']=Hash::make($request->password);
    	if ($request->hasFile('gambar')) {
    		$file=$request->file('gambar');
    		$direktori=public_path().'/gambar/pelatih/';
    		$nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
    		$uploadSuccess=$file->move($direktori,$nama_file);
    		$pelatih['gambar']=$nama_file;
    	}
    
    	$this->pelatih->create($pelatih);
        Session::flash('message','data berhasil disimpan');
        return redirect('admin/pelatih');
    }

    public function edit($id){
        $pelatih=$this->pelatih->find($id);
        $cabor=$this->cabor->all()->pluck('nama_cabor','id');
        $provinsi=$this->provinsi->all()->pluck('nama_provinsi','id');
        return view('Admin.pelatih.ubah',compact('pelatih','cabor','provinsi'));
    }

    public function update($id,Request $request){
        $region=$this->pelatih->find($id);
        $this->validate($request,[
            'nama'=>'required',
            'email'=>'required|email',
            
            'gambar'=>'mimes:JPG,JPEG,PNG,jpg,jpeg,png',
            'alamat_rinci'=>'required',
            'tgl_lahir'=>'required|date|before:today',
            'pelatih_id'=>'required',
            'kelamin'=>'required',
            'provinsi_id'=>'required',
            
            'no_hp'=>'required',
            'agama'=>'required',
            ]);
        $pelatih=$request->all();
        if ($request->hasFile('gambar')) {
            $file=$request->file('gambar');
            $direktori=public_path().'/gambar/pelatih/';
            File::delete($direktori.$this->pelatih->find($id)->gambar);            
            $nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
            $uploadSuccess=$file->move($direktori,$nama_file);
            $pelatih['gambar']=$nama_file; 
        }else{
            $pelatih['gambar']=$this->pelatih->find($id)->gambar;
        }

        if ($request->has('password')) {
            $pelatih['password']=Hash::make($request->password);
        }else{
            $pelatih['password']=$this->pelatih->find($id)->password;
        }

        if ($request->has('kabupaten_id')) {
            $pelatih['kabupaten_id']=$request->kabupaten_id;
        }else{
            $pelatih['kabupaten_id']=$region->kabupaten_id;
        }
        if ($request->has('kota_id')) {
            $pelatih['kota_id']=$request->kota_id;
        }else{
            $pelatih['kota_id']=$region->kota_id;
        }
        if ($request->has('kecamatan_id')) {
            $pelatih['kecamatan_id']=$request->kecamatan_id;
        }else{
            $pelatih['kecamatan_id']=$region->kecamatan_id;
        }
        if ($request->has('kelurahan_id')) {
            $pelatih['kelurahan_id']=$request->kelurahan_id;
        }else{
            $pelatih['kelurahan_id']=$region->kelurahan_id;
        }

        $this->pelatih->find($id)->update($pelatih);
       Session::flash('message','data berhasil disimpan');
        return redirect('admin/pelatih');
    }

    public function destroy($id){
        $this->pelatih->destroy($id);
        Session::flash('message','data berhasil dihapus');
        return redirect('admin/pelatih');
    }
}
