<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CaborPrestasi;
use App\Model\Cabor;
use Session;
class CaborPrestasiController extends Controller
{
     public function __construct(CaborPrestasi $cabprestasi,Cabor $cabor){
        $this->cabprestasi=$cabprestasi;
        $this->cabor=$cabor;
        $this->middleware('auth:admin');
    }

    public function index(){
        $cabprestasi=$this->cabprestasi->orderBy('id','desc')->get();
        return view('Admin.caborprestasi.index',compact('cabprestasi'));
    }
    public function create(){
        $cabor=$this->cabor->pluck('nama_cabor','id');
        return view('Admin.caborprestasi.tambah',compact('cabor'));
    }
    public function store(Request $request){
        $this->validate($request,[
            'cabor_id'=>'required|unique:cabor_prestasi',           
            ]);
        $cabprestasi=$request->all();
        $data=$this->cabprestasi->create($cabprestasi);
        Session::flash('message','data cabor "'.$data->cabor->nama_cabor.'" berhasil disimpan');
        return redirect('admin/caborprestasi');
    }
    public function edit($id){
         $cabor=$this->cabor->pluck('nama_cabor','id');
         $cabprestasi=$this->cabprestasi->findOrFail($id);
         return view('Admin.caborprestasi.ubah',compact('cabor','cabprestasi'));
    }
    public function update($id,Request $request){
        $this->validate($request,[
            'cabor_id'=>'required',           
            ]);
        $cabprestasi=$request->all();
        $data=$this->cabprestasi->findOrFail($id)->update($cabprestasi);
        Session::flash('message','data cabor berhasil diubah');
        return redirect('admin/caborprestasi');
    }
    public function show(){

    }
    public function destroy($id){
    	$this->cabprestasi->destroy($id);
        Session::flash('message','data berhasil dihapus');
        return redirect()->back();
    }
}
