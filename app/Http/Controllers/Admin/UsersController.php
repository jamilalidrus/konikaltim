<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Cabor;
use File,Hash,Session;
use App\Model\Provinsi;
use App\Model\Kabupaten;
use App\Model\Kota;
use App\Model\Kecamatan;
use App\Model\Kelurahan;
use DB;
use Yajra\Datatables\Datatables;
use App\Model\KotaDomisili;
class UsersController extends Controller
{

	public function __construct(User $user,Cabor $cabor,Provinsi $provinsi,Kabupaten $kabupaten,Kota $kota,Kecamatan $kecamatan,Kelurahan $kelurahan){
		$this->user=$user;
		$this->cabor=$cabor;
		$this->provinsi=$provinsi;
		$this->kabupaten=$kabupaten;
		$this->kota=$kota;
		$this->kecamatan=$kecamatan;
		$this->kelurahan=$kelurahan;
		$this->middleware('auth:admin');
	}



	public function index(Request $request){
		
		$user=$this->user->orderBy('id','desc')->filter($request)->paginate(20);
		$cabor=$this->cabor->all()->pluck('nama_cabor','id');
		return view('Admin.user.index',compact('user','cabor'));
	}

	public function create(){
		$cabor=$this->cabor->all()->pluck('nama_cabor','id');
		$kabkota=Provinsi::find(4)->kabupaten->pluck('nama_kabupaten','id');
		$provinsi=$this->provinsi->all()->pluck('nama_provinsi','id');
		
		return view('Admin.user.tambah',compact('cabor','provinsi','kabupaten','kabkota'));
	}

	public function store(Request $request){

		$this->validate($request,[
			'name'=>'required',
			'email'=>'required|email',
			'password'=>'required',
			'gambar'=>'required|mimes:JPG,JPEG,PNG,jpg,jpeg,png',
			'tgl_lahir'=>'required|date|before:today',
			'kelamin'=>'required',
			'ayah'=>'required',
			'ibu'=>'required',
			'no_hp'=>'required',
			'berat_badan'=>'required',
			'olahraga_id'=>'required',
			'status_atlit'=>'required',
			'agama'=>'required',
			
			'tinggi'=>'required',
			'darah'=>'required',
			'provinsi_id'=>'required',
			'kabupaten_id'=>'required',
			'kota_id'=>'required',
			'kecamatan_id'=>'required',
			'kelurahan_id'=>'required',
			'sekolah'=>'required',
			'kelas'=>'required',
			'alamat_sekolah'=>'required',
			'sepatu'=>'required',
			'kaos'=>'required',
			'kameja'=>'required',
			'hobi'=>'required',

			]);

		$umur= date("Y")-substr($request->tgl_lahir,0,4);	
		
			$user=$request->all();
			$user['password']=Hash::make($request->password);
			$username=strtolower($request->name);
			$user['username']=str_replace(' ','', $username);
			$user['status']='accepted';
			if ($request->hasFile('gambar')) {
				$file=$request->file('gambar');
				$direktori=public_path().'/gambar/user/';			
				$nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
				$uploadSuccess=$file->move($direktori,$nama_file);
				$user['gambar']=$nama_file;	
			}

			$this->user->create($user);
			Session::flash('message','data '.$request->name.' berhasil disimpan');
			return redirect('admin/atlit');
		
		
	}
	public function edit($id){
		$cabor=$this->cabor->all()->pluck('nama_cabor','id');
		$user=$this->user->find($id);
		$kabkota=Provinsi::find(4)->kabupaten->pluck('nama_kabupaten','id');
		$provinsi=$this->provinsi->all()->pluck('nama_provinsi','id');
		return view('Admin.user.ubah',compact('user','cabor','provinsi','kabkota'));
	}


	public function update(Request $request, $id){
		$region=$this->user->find($id);
		$this->validate($request,[
			'name'=>'required',
			'email'=>'required|email',			
			'gambar'=>'mimes:JPG,JPEG,PNG,jpg,jpeg,png',
			'tgl_lahir'=>'required|date|before:today',
			'kelamin'=>'required',
			'ayah'=>'required',
			'ibu'=>'required',
			'no_hp'=>'required',
			'berat_badan'=>'required',
			'olahraga_id'=>'required',
			'status_atlit'=>'required',
			'agama'=>'required',
			
			'tinggi'=>'required',
			'darah'=>'required',
			'provinsi_id'=>'required',			
			'sekolah'=>'required',
			'kelas'=>'required',
			'alamat_sekolah'=>'required',
			'sepatu'=>'required',
			'kaos'=>'required',
			'kameja'=>'required',
			'hobi'=>'required',

			]);
		$user=$request->all();
		$user['username']=$this->user->find($id)->username;
		$user['status']='accepted';
		if ($request->hasFile('gambar')) {
			$file=$request->file('gambar');
			$direktori=public_path().'/gambar/user/';
			File::delete($direktori.$this->user->find($id)->gambar);			
			$nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
			$uploadSuccess=$file->move($direktori,$nama_file);
			$user['gambar']=$nama_file;	
		}else{
			$user['gambar']=$this->user->find($id)->gambar;
		}

		if ($request->has('password')) {
			$user['password']=Hash::make($request->password);
		}else{
			$user['password']=$this->user->find($id)->password;
		}

		if ($request->has('kabupaten_id')) {
			$user['kabupaten_id']=$request->kabupaten_id;
		}else{
			$user['kabupaten_id']=$region->kabupaten_id;
		}
		if ($request->has('kota_id')) {
			$user['kota_id']=$request->kota_id;
		}else{
			$user['kota_id']=$region->kota_id;
		}
		if ($request->has('kecamatan_id')) {
			$user['kecamatan_id']=$request->kecamatan_id;
		}else{
			$user['kecamatan_id']=$region->kecamatan_id;
		}
		if ($request->has('kelurahan_id')) {
			$user['kelurahan_id']=$request->kelurahan_id;
		}else{
			$user['kelurahan_id']=$region->kelurahan_id;
		}


		$this->user->find($id)->update($user);
		Session::flash('message','data "'.$request->name.'" berhasil diubah');
		return redirect('admin/atlit');
	}

	public function destroy($id){
		$direktori=public_path().'/gambar/user/';
		File::delete($direktori.$this->user->find($id)->gambar);
		$this->user->destroy($id);
		Session::flash('message','data berhasil dihapus');
		return redirect('admin/atlit');
	}

public function cari(Request $request){
		$user=$this->user->where('name','like%',$request)->paginate(20);
		return view('Admin.user.index',compact('user'));
}

	

	public function atlit(){
		DB::statement(DB::raw('set @rownum=0'));
        $user=User::all(DB::raw('@rownum := @rownum + 1 AS number '),'id','name','username','email','gambar','tgl_lahir','kelamin','ayah','ibu','no_hp','berat_badan','agama','status','tinggi','darah','sekolah','kelas','alamat_sekolah','sepatu','kaos','kameja','hobi');
        return Datatables::of($user)
      
        ->addColumn('gambar',function($user){
        		return '
        		<a class="btn btn-primary" data-toggle="modal" data-target="#myModal'.$user->id.'"><i class="fa fa-image"></i> Show</a> 
        		
        	 <!-- modal gambar -->
              <div class="modal inmodal fade" id="myModal'.$user->id.'" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title">'.$user->nama.'</h4>
                          <small class="font-bold"></small>
                        </div>
                        <div class="modal-body">
                          <img class="img img-responsive img-thumbnail" src="'.asset("gambar/user/$user->gambar").'">
                        </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
              <!-- endmodal gambar -->
        		
        		';
        })
        ->addColumn('action',function($user){
        	return'<form method="POST" action="'.route('atlit.destroy',$user->id).'" onsubmit="return confirm()">
                    <a href="atlit/'.$user->id.'/edit" class="btn  btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <button type="submit" value="DELETE!" class="btn btn-danger" >Hapus</button>
                </form>';
        })
        ->addColumn('kelamin',function($user){
        	if ($user->kelamin == 1) {
        		return 'laki -laki';
        	}elseif($user->kelamin == 0){
        		return 'perempuan';
        	}
        })
        ->addColumn('cabang',function($user){
        	return $user->find($user->id)->cabor->nama_cabor;
        })
        ->make(true);
	}




}
