<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\VisiMisi;
use Session;
class VisimisiController extends Controller
{
     public function __construct(VisiMisi $visimisi){
        $this->visimisi=$visimisi;
        $this->middleware('auth:admin');
    }

    public function index(){
        $visimisi=$this->visimisi->all();
        return view('Admin.visimisi.index',compact('visimisi'));
    }
    public function create(){
        $status=$this->visimisi->all()->count();
        if ($status==0) {
            return view('Admin.visimisi.tambah');
        }elseif($status > 0){
            Session::flash('message','data visi dan misi sudah ada');
            return redirect('admin/visimisi');
        }
        
    }
    public function store(Request $request){
        $this->validate($request,[
            'text'=>'required',
            ]);
        $visimisi=$request->all();
        $this->visimisi->create($visimisi);
        Session::flash('message','data berhasil disimpan');
        return redirect('admin/visimisi');

    }
    public function edit($id){
        $status=1;
        $visimisi=$this->visimisi->findOrFail($id);
        return view('Admin.visimisi.ubah',compact('visimisi'));
    }
    public function update($id,Request $request){
         $this->validate($request,[
            'text'=>'required',
            ]);
        $visimisi=$request->all();
        $this->visimisi->findOrFail($id)->update($visimisi);
        Session::flash('message','data berhasil diubah');
        return redirect('admin/visimisi');
    }
    public function show(){

    }
    public function destroy($id){
    	$this->visimisi->destroy($id);
        Session::flash('message','data berhasil dihapus');
        return redirect()->back();
    }
}
