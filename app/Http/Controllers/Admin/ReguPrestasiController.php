<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Regu;
use App\Model\Medali;
use App\Model\ReguPrestasi;
use Session;
class ReguPrestasiController extends Controller
{
    public function __construct(Regu $regu, Medali $medali,ReguPrestasi $reguprestasi){
        $this->medali=$medali;
        $this->regu=$regu;
        $this->reguprestasi=$reguprestasi;
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prestasiregu=$this->reguprestasi->orderBy('id','desc')->paginate(20);
        return view('Admin.prestasiregu.index',compact('prestasiregu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $medali=$this->medali->pluck('medali','id');
        $regu=$this->regu->all();

        foreach ($regu as $key)
        $namaregu[$key->id] ="{$key->cabor->nama_cabor} ({$key->kota->nama_kota})";           
        return view('Admin.prestasiregu.tambah',compact('medali','namaregu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'regu_id'=>'required',
            'medali_id'=>'required',
        ]);
        $reguprestasi=$request->all();
        $this->reguprestasi->create($reguprestasi);
        Session::flash('message','data berhasil disimpan');
        return redirect('admin/reguprestasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $medali=$this->medali->pluck('medali','id');
        $regu=$this->regu->all();
       $prestasiregu=$this->reguprestasi->findOrFail($id);

        foreach ($regu as $key)
        $namaregu[$key->id] ="{$key->cabor->nama_cabor} ({$key->kota->nama_kota})";           
        return view('Admin.prestasiregu.tambah',compact('medali','namaregu','prestasiregu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'regu_id'=>'required',
            'medali_id'=>'required',
        ]);
        $reguprestasi=$request->all();
        $this->reguprestasi->find($id)->update($reguprestasi);
        Session::flash('message','data berhasil diubah');
        return redirect('admin/reguprestasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->reguprestasi->destroy($id);
        Session::flash('message','data berhasil dihapus');
        return redirect()->back();
    }
}
