<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Pertandingan;
use App\Model\Tahun;
use Session,Auth;
class PertandinganController extends Controller
{
    public function __construct(Pertandingan $pertandingan, Tahun $tahun){
    	$this->pertandingan=$pertandingan;
    	$this->tahun=$tahun;
        $this->middleware('auth:admin');
    }

    public function index(){
    	$pertandingan=$this->pertandingan->all();
    	return view('Admin.pertandingan.index',compact('pertandingan'));
    }
     public function create(){
     	$tahun=$this->tahun->where('status','active')->pluck('tahun','id');
    	return view('Admin.pertandingan.tambah',compact('tahun'));
    }
     public function store(Request $request){
    	$this->validate($request,[
    		'nama'=>'required',
    		'tahun_id'=>'required',
    		]);
    	$pertandingan=$request->all();
    	$pertandingan['status']='nonactive';
    	$this->pertandingan->create($pertandingan);
    	Session::flash('message','data pertandingan berhasil ditambahkan');
    	return redirect('admin/pertandingan');

    }
     public function edit($id){
    	$pertandingan=$this->pertandingan->findOrFail($id);
    	$tahun=$this->tahun->where('status','active')->pluck('tahun','id');
    	return view('Admin.pertandingan.ubah',compact('pertandingan','tahun'));
    }
     public function update($id, Request $request){

    	$this->validate($request,[
    		'nama'=>'required',
    		'tahun_id'=>'required',
    		]);
    	$pertandingan=$request->all();
    	$this->pertandingan->find($id)->update($pertandingan);
    	Session::flash('message','data pertandingan berhasil diubah');
    	return redirect('admin/pertandingan');
    }
     public function destroy($id){
    	$this->pertandingan->destroy($id);
    	Session::flash('message','data pertandingan berhasil dihapus');
    	return redirect('admin/pertandingan');
    }

    public function show($id){
    	$pertandingan=$this->pertandingan->findOrFail($id);
    	if ($pertandingan->status == 'active') {
    		$pertandingan->status='nonactive';
    	}elseif($pertandingan->status == 'nonactive'){
    		$pertandingan->status='active';
    	}
    	$pertandingan->update();
    	Session::flash('message','status pertandingan berhasil diubah');
    	return redirect('admin/pertandingan');
    }
}
