<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Model\Tahun;
class TahunController extends Controller
{
    public function __construct(Tahun $tahun){
        $this->tahun=$tahun;
        $this->middleware('auth:admin');        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahun=$this->tahun->orderBy('id','desc')->get();
        return view('Admin.tahun.index',compact('tahun'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.tahun.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'tahun'=>'required|unique:tahun',
            ]);
        $tahun=$request->all();
        $this->tahun->create($tahun);
        Session::flash('message','data berhasil disimpan');
        return redirect('admin/tahun');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tahun=$this->tahun->findOrFail($id);
        if ($tahun->status == 'active') {
            $tahun['status']='nonactive';
           
        }elseif($tahun->status == 'nonactive'){           
             
                $tahun['status']='active';                
        }
        $tahun->update();
        Session::flash('message','status tahun berhasil diubah');
        return redirect('admin/tahun');
        
    }   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tahun=$this->tahun->findOrFail($id);
        return view('Admin.tahun.ubah',compact('tahun'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'tahun'=>'required',
            ]);
        $tahun=$request->all();
        $this->tahun->findOrFail($id)->update($tahun);
        Session::flash('message','data berhasil diubah');
        return redirect('admin/tahun');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->tahun->destroy($id);
        Session::flash('message','data berhasil dihapus');
        return redirect('admin/tahun');
    }
}
