<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PengumumanAdmin;
use Session;
class PengumumanController extends Controller
{

    public function __construct(PengumumanAdmin $pengumuman){
        $this->middleware('auth:admin');
        $this->pengumuman=$pengumuman;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengumuman=$this->pengumuman->orderBy('id','desc')->paginate(20);
        return view('Admin.pengumuman.index',compact('pengumuman'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.pengumuman.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'judul'=>'required',
            'isi'=>'required',
        ]);
        $pengumuman=$request->all();
        $url=str_replace(' ','-', $request->judul);
        $pengumuman['url']=str_replace('/','-',$url).'.html';
        $this->pengumuman->create($pengumuman);
        Session::flash('message','pengumuman berhasil dibuat');
        return redirect('admin/pengumumanadmin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengumuman=$this->pengumuman->findOrFail($id);
         return view('Admin.pengumuman.ubah',compact('pengumuman'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'judul'=>'required',
            'isi'=>'required',
        ]);
        $pengumuman=$request->all();
        $url=str_replace(' ','-', $request->judul);
        $pengumuman['url']=str_replace('/','-',$url).'.html';
        $this->pengumuman->find($id)->update($pengumuman);
        Session::flash('message','pengumuman berhasil diubah');
        return redirect('admin/pengumumanadmin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->pengumuman->destroy($id);
          Session::flash('message','pengumuman berhasil dihapus');
        return redirect('admin/pengumumanadmin');
    }
}
