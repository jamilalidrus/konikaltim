<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Berita;
use Session,File;
class BeritaController extends Controller
{
    public function __construct(Berita $berita){
        $this->berita=$berita;
        $this->middleware('auth:admin');
    }

    public function index(){
        $berita=$this->berita->orderBy('id','desc')->paginate(12);
        return view('Admin.berita.index',compact('berita'));
    }
    public function create(){
        return view('Admin.berita.tambah');
    }
    public function store(Request $request){
        $this->validate($request,[
            'judul'=>'required',
            'gambar'=>'required|mimes:JPG,JPEG,PNG,MOV,jpg,jpeg,png,mov|max:2000',
            'text'=>'required',
            ]);
        $berita=$request->all();
        $url=str_replace(' ','-', $request->judul);
        $berita['url']=str_replace('/','-',$url).'.html';
        if ($request->hasFile('gambar')) {
                $file=$request->file('gambar');
                $direktori=public_path().'/gambar/news/';           
                $nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
                $uploadSuccess=$file->move($direktori,$nama_file);
                $berita['gambar']=$nama_file; 
            }
        $this->berita->create($berita);
        Session::flash('message','data berita "'.$request->judul.'" berhasil disimpan');
        return redirect('admin/berita');
    }
    public function edit($id){
        $berita=$this->berita->findOrFail($id);
        return view('Admin.berita.ubah',compact('berita'));
    }
    public function update($id,Request $request){
         $this->validate($request,[
            'judul'=>'required',
            'gambar'=>'mimes:JPG,JPEG,PNG,MOV,jpg,jpeg,png,mov|max:2000',
            'text'=>'required',
            ]);
        $berita=$request->all();
        $url=str_replace(' ','-', $request->judul);
        $berita['url']=str_replace('/','-',$url).'.html';
        if ($request->hasFile('gambar')) {
                $file=$request->file('gambar');
                $direktori=public_path().'/gambar/news/';  
                File::delete($direktori.$this->berita->findOrFail($id)->gambar);         
                $nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
                $uploadSuccess=$file->move($direktori,$nama_file);
                $berita['gambar']=$nama_file; 
            }else{
                $berita['gambar']=$this->berita->findOrFail($id)->gambar;
            }
        $this->berita->findOrFail($id)->update($berita);
        Session::flash('message','data berita "'.$request->judul.'" berhasil diubah');
        return redirect('admin/berita');
    }
    public function show(){

    }
    public function destroy($id){
        $direktori=public_path().'/gambar/news/';  
        File::delete($direktori.$this->berita->findOrFail($id)->gambar);
    	$this->berita->destroy($id);
        Session::flash('message','data berita berhasil dihapus');
        return redirect()->back();
    }
}
