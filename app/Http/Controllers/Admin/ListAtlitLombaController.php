<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AtlitPertandingan;
use App\Model\Pertandingan;
use Session;
class ListAtlitLombaController extends Controller
{

	public function __construct(AtlitPertandingan $atlitpertandingan,Pertandingan $pertandingan){
		$this->atlitpertandingan=$atlitpertandingan;
		$this->pertandingan=$pertandingan;
		$this->middleware('auth:admin');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    	$pertandingan=$this->pertandingan->where('status','active')->orderBy('id','desc')->paginate(20);
        return view('Admin.listatlitlomba.index',compact('pertandingan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertandingan=$this->pertandingan->findOrFail($id);

        $atlitpertandingan=$this->atlitpertandingan->where('pertandingan_id',$pertandingan->id)
        ->orderBy('id','desc')->paginate(100);
       
        return view('Admin.listatlitlomba.show',compact('atlitpertandingan','pertandingan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->atlitpertandingan->destroy($id);
        Session::flash('message','atlit berhasil dibatalkan');
        return redirect()->back();
    }
}
