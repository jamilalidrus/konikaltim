<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sejarah;
use Session;
class SejarahController extends Controller
{
     public function __construct(Sejarah $sejarah){
        $this->sejarah=$sejarah;
        $this->middleware('auth:admin');
    }

    public function index(){
        $sejarah=$this->sejarah->all();
        return view('Admin.sejarah.index',compact('sejarah'));
    }
    public function create(){
        $status=$this->sejarah->all()->count();
        if ($status==0) {
             return view('Admin.sejarah.tambah');
        }elseif($status >= 0){
            Session::flash('message','data sejarah sudah ada');
            return redirect('admin/sejarah');
        }
       
    }
    public function store(Request $request){
        $this->validate($request,[
            'text'=>'required',
            ]);
        $sejarah=$request->all();
        $this->sejarah->create($sejarah);
        Session::flash('message','data sejarah berhasil disimpan');
        return redirect('admin/sejarah');
    }
    public function edit($id){
        $sejarah=$this->sejarah->findOrFail($id);
        return view('Admin.sejarah.ubah',compact('sejarah'));
    }
    public function update($id,Request $request){
        $this->validate($request,[
            'text'=>'required',
            ]);
        $sejarah=$request->all();
        $this->sejarah->findOrFail($id)->update($sejarah);
        Session::flash('message','data sejarah berhasil diubah');
        return redirect('admin/sejarah');
    }
    public function show(){

    }
    public function destroy($id){
    	$this->sejarah->destroy($id);
        Session::flash('message','data berhasil dihapus');
        return redirect()->back();
    }
}
