<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Berita;
use App\Model\VisiMisi;
use App\Model\Sejarah;
use App\Model\PengumumanAdmin;
class IndexController extends Controller
{
	public function __construct(Berita $berita,PengumumanAdmin $pengumuman){
		$this->berita=$berita;
        $this->pengumuman=$pengumuman;
	}
    public function index(){
    	$berita=$this->berita->orderBy('id','desc')->paginate(10);
    	return view('homepage.index',compact('berita'));
    }

    public function show($id,$url){
    	$berita=$this->berita->where(['id'=>$id,'url'=>$url])->first();
    	return view('homepage.read-berita',compact('berita'));
    }

    public function visimisi(){
        $visimisi=VisiMisi::first();
        $sejarah=Sejarah::first();
        return view('homepage.sejarahvisimisi',compact('visimisi','sejarah'));
    }

    public function allnews(){
       $berita=$this->berita->orderBy('id','desc')->paginate(12);
        return view('homepage.all-news',compact('berita'));
    }

    public function pengumuman(){
        $pengumuman=$this->pengumuman->orderBy('id','desc')->paginate(20);
        return view('homepage.pengumuman',compact('pengumuman'));
    }

    public function readpengumuman($id,$url){
        $pengumuman=$this->pengumuman->where(['id'=>$id,'url'=>$url])->first();
         return view('homepage.read-pengumuman',compact('pengumuman'));
    }

    public function bank(){
        return view('homepage.bank');
    }

}
