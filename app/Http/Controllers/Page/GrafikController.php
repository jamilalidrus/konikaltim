<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cabor;
use App\User;
use Carbon;
use App\Model\Medali;
use App\Pelatih;
class GrafikController extends Controller
{

	public function __construct(Cabor $cabor,User $user,Medali $medali, Pelatih $pelatih){
		$this->cabor=$cabor;
        $this->user=$user;
        $this->medali=$medali;
		$this->pelatih=$pelatih;
	}


    public function grafik(){
    	$cabor=$this->cabor->orderBy('id','asc')->get();
	return view('homepage.grafik.grafik', compact('cabor'));
    }

    public function byAge(){
    	
    	  	
    
    	$muda=[];
    	$dewasa=[];
    	$tua=[];
    	$user=$this->user->all();

    	foreach($user as $a)

    		if (Carbon\Carbon::now()->year - Carbon\Carbon::parse($a->tgl_lahir)->year < 19 ) {
    			$muda[]= $a->id;
    		}
    		$muda= collect($muda)->count();

    	foreach($user as $a)
    		if (Carbon\Carbon::now()->year - Carbon\Carbon::parse($a->tgl_lahir)->year > 18 and  Carbon\Carbon::now()->year - Carbon\Carbon::parse($a->tgl_lahir)->year < 26 ) {
    			$dewasa[]=$a->id;
    		}
			$dewasa= collect($dewasa)->count();
			// return $dewasa;
		foreach($user as $a)
    		if (Carbon\Carbon::now()->year - Carbon\Carbon::parse($a->tgl_lahir)->year > 29) {
    			$tua[]= $a->id;
    		}	
   		
    		
    		$tua= collect($tua)->count();
    		// return $tua;
    		
    		
    		
    
    		

    	
    	return view('homepage.grafik.byAge',compact('muda','dewasa','tua'));

    }


    public function bypiala(){

        $medali=Medali::with('prestasi')->get();
        return view('homepage.grafik.bypiala',compact('medali'));
    }

    public function pelatih(){
        $cabor=$this->cabor->orderBy('id','asc')->get();
    return view('homepage.grafik.pelatih', compact('cabor'));

    }
}
