<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Requests\PendaftaranRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Cabor;
use App\Model\Provinsi;
use App\Model\Sertifikat;
use Hash,Session;
class PendaftaranController extends Controller
{
    public function __construct(User $user,Cabor $cabor,Provinsi $provinsi,Sertifikat $sertifikat){
    	$this->user=$user;
    	$this->cabor=$cabor;
    	$this->provinsi=$provinsi;
        $this->sertifikat=$sertifikat;
    }

    public function index(){
    	$cabor=$this->cabor->all()->pluck('nama_cabor','id');
        $kabkota= $this->provinsi->findOrFail(4)->kabupaten->pluck('nama_kabupaten','id');
    	$provinsi=[''=>'Select Option'];
        foreach ($this->provinsi->all() as $pr)
        $provinsi[$pr->id]="{$pr->nama_provinsi}";
    	return view('homepage.pendaftaran',compact('cabor','provinsi','kabkota'));
    }

    public function store(PendaftaranRequest $request){
    	 $user=$request->all();
         $umur= date("Y")-substr($request->tgl_lahir,0,4);  
        if($umur <= 14 ){
            Session::flash('message','umur anda belum mencukupi, umur mininal pendaftaran adalah 13 tahun');
            return redirect()->back()->withInput($request->all());
        }else{
           $user['password']=Hash::make($request->password);
         $user['status']='waiting';
         $username=strtolower($request->name);
         $user['username']=str_replace(' ','', $username);
        if ($request->hasFile('gambar')) {
            $file=$request->file('gambar');
            $direktori=public_path().'/gambar/user/';           
            $nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
            $uploadSuccess=$file->move($direktori,$nama_file);
            $user['gambar']=$nama_file;    
            }      
        $data=$this->user->create($user);
        $sertifikat=$request->all();
          if ($request->hasFile('nama_sertifikat')) {
            $file=$request->file('nama_sertifikat');
            $direktori=public_path().'/file/sertifikat/';           
            $nama_file=str_random(20).'.'.$file->getClientOriginalExtension();
            $uploadSuccess=$file->move($direktori,$nama_file);
            $sertifikat['nama_sertifikat']=$nama_file;
            $sertifikat['user_id']=$data->id;
        }
        $this->sertifikat->create($sertifikat);
        Session::flash('message','data anda berhasil di daftarkan, silahkan login ke akun anda notofikasi akan diterima jika anda diterima sebagai atlit.');
        return redirect('home/direct'); 
        }
    	 
    }

    public function direct(){
        return view('homepage.redirect');
    }
}
