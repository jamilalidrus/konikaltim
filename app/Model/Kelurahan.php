<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table='kelurahan';
    protected $fillable=['nama_kelurahan','kecamatan_id'];

    public function kecamatan(){
    	return $this->belongsTo(Kecamatan::class);
    }

    public function user(){
    	return $this->hasMany(App\User::class,'id','kelurahan_id');
    }
}
