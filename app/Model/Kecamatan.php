<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table='kecamatan';
    protected $fillable=['nama_kecamatan','kota_id'];

    public function kota(){
    	return $this->belongsTo(Kota::class);
    }
    public function kelurahan(){
    	return $this->hasMany(Kelurahan::class,'id','kecamatan_id');
    }

    public function user(){
    	return $this->hasMany(App\User::class,'id','kecamatan_id');
    }
}
