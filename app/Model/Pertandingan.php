<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pertandingan extends Model
{
    protected $table='pertandingan';
    protected $fillable=['nama','tahun_id','status'];
    public $timestamps=false;

    public function tahun(){
    	return $this->belongsTo(Tahun::class);
    }

    public function atlitpertandingan(){
    	return $this->hasMany(AtlitPertandingan::class);
    }
}
