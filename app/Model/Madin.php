<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Madin extends Model
{
    protected $table='madin';
    protected $fillable=['judul','isi'];
}
