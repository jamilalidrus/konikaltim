<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    protected $table='kabupaten';
    protected $fillable=['nama_kabupaten','provinsi_id'];

    public function provinsi(){
    	return $this->belongsTo(Provinsi::class);
    }
    public function kota(){
    	return $this->hasMany(Kota::class,'id','kabupaten_id');
    }

    public function user(){
    	return $this->hasMany(App\User::class,'id','kabupaten_id');
    }
}
