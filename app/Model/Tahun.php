<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tahun extends Model
{
    protected $table='tahun';
    protected $fillable=['tahun'];

    public $timestamps=false;

    public function pertandingan(){
    	return $this->hasMany(Pertandingan::class);
    }
}
