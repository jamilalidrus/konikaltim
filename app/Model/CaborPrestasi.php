<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CaborPrestasi extends Model
{
   protected $table='cabor_prestasi';
   protected $fillable=['cabor_id','perak','perunggu'];

   public function cabor(){
   	return $this->belongsTo(Cabor::class,'cabor_id');
   }

   
}
