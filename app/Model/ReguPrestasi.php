<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReguPrestasi extends Model
{
    protected $table='prestasiregu';
    protected $fillable=['regu_id','medali_id'];

    public function regu(){
    	return $this->belongsTo(Regu::class);
    }
    public function medali(){
    	return $this->belongsTo(Medali::class);
    } 
}
