<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Medali extends Model
{
    protected $table='medali';
    protected $fillable=['medali'];

    public function user(){
    	return $this->hasMany('App\User');
    }

    public function prestasi(){
    	return $this->hasMany(Prestasi::class,'piala_id','id');
    }

    public function prestasiregu(){
    	return $this->hasMany(ReguPrestasi::class);
    }
}
