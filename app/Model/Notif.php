<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notif extends Model
{
    protected $table='notif';
    protected $fillable=['isi','user_id','pelatih','status'];

    public function user(){
    	return $this->belongsTo(App\User::class);
    }

    public function pelatih(){
    	return $this->belongsTo(App\Pelatih::class);
    }
}
