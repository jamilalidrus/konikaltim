<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table='provinsi';
    protected $fillable=['nama_provinsi'];

    public function kabupaten(){
    	return $this->hasMany(Kabupaten::class,'provinsi_id','id');
    }

    public function user(){
    	return $this->hasMany(App\User::class,'id','provinsi_id');
    }

    public function pelatih(){
    	return $this->hasMany(App\Pelatih::class,'id','provinsi_id');
    }
}
