<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ListLombaAtlit extends Model
{
    protected $table='atlit_pertandingan';
    protected $fillable=['user_id','pertandingan_id','tahun_id'];
}
