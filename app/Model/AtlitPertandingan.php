<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AtlitPertandingan extends Model
{
    protected $table='atlit_pertandingan';
    protected $fillable=['user_id','pertandingan_id','pelatih_id','provinsi_id'];

    public $timestamps=false;

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function pelatih(){
    	return $this->belongsTo('App\Pelatih');
    }

    public function pertandingan(){
    	return $this->belongsTo(Pertandingan::class);
    }

    public function cabor(){
        return $this->belongsTo(Cabor::class);
    }
}
