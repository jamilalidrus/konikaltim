<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sertifikat extends Model
{
    protected $table='sertifikat';
    protected $fillable=['nama_sertifikat','user_id'];

    public function user(){
    	return $this->belongsTo(App\User::class);
    }
}
