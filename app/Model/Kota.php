<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table='kota';
    protected $fillable=['nama_kota','kabupaten_id'];

    public function kabupaten(){
    	return $this->belongsTo(Kabupaten::class);
    }

    public function kecamatan(){
    	return $this->hasMany(Kecamatan::class,'id','kecamatan_id');
    }

    public function user(){
    	return $this->hasMany(App\User::class,'id','kota_id');
    }

    public function regu(){
        return $this->hasMany(Regu::class);
    }
}
