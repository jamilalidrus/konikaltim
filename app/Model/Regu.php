<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Regu extends Model
{
    protected $table='regu';
    protected $fillable=['cabor_id','kota_id'];

    public function cabor(){
    	return $this->belongsTo(Cabor::class);
    }

    public function kota(){
    	return $this->belongsTo(Kota::class);
    }

    public function prestasi(){
    	return $this->hasMany(ReguPrestasi::class);
    }
}
