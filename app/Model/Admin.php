<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table='admins';
    protected $fillable=['email','password','gambar','name'];
    public $timestamps=false;

    public function news(){
    	return $this->hasMany(News::class);
    }
}
