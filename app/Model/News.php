<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table='news';
    protected $fillable=['judul','isi','gambar','admin_id','url'];

    public function comment(){
    	return $this->hasMany(NewsComment::class);
    }

    public function admin(){
    	return $this->belongsTo(Admin::class,'admin_id');
    }
}
