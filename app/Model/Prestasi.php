<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Prestasi extends Model
{
    protected $table='prestasi';
    protected $fillable=['user_id','cabor_id','piala_id'];


    public function cabor(){
    	return $this->belongsTo(Cabor::class,'cabor_id');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function piala(){
    	return $this->belongsTo(Medali::class);
    }

    public function scopeFilter($query,$request){

        if ($request->has('cabor')) {
            $query->where('cabor_id',$request->get('cabor'));
        }
        if ($request->has('medali')) {
            $query->where('piala_id',$request->get('medali'));
        }
    }
}
