<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pengumuman extends Model
{
    protected $table='pengumuman';
    protected $fillable=['pelarih_id','cabor_id','isi','tujuan'];

   	public function pelatih(){
   		return $this->belongsTo('App\Pelatih');
   	}
}
