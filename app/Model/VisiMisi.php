<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VisiMisi extends Model
{
    protected $table='visimisi';
    protected $fillable=['text'];
}
