<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KotaDomisili extends Model
{
    protected $table='kota_domisili';
    protected $fillable=['nama_kota'];

    public function user(){
    	return $this->hasMany('App\User','kota_domisili','id');
    } 
}
