<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cabor extends Model
{
    protected $table='cabor';
    protected $fillable=['nama_cabor'];

    public function prestasi(){
    	return $this->hasMany(Prestasi::class);
    }

    public function caborprestasi(){
    	return $this->hasMany(CaborPrestasi::class);
    }
    public function user(){
    	return $this->hasMany('App\User','olahraga_id','id');
    }

    public function pelatih(){
        return $this->hasMany('App\Pelatih','pelatih_id','id');
    }

    public function lombaatlit(){
        return $this->hasMany(AtlitPertandingan::class);
    }

    public function regu(){
        return $this->hasMany(Regu::class);
    }
}
