<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PengumumanAdmin extends Model
{
    protected $table='pengumuman_admin';
    protected $fillable=['judul','isi','url'];
}
