<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pelatih extends Authenticatable
{
    use Notifiable;
    protected $guard='pelatih';
    protected $table='pelatihs';
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama','email','gambar','password','tgl_lahir','pelatih_id','kelamin','alamat_rinci','provinsi_id','kabupaten_id','kota_id','kecamatan_id','kelurahan_id','agama','no_hp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function provinsi(){
        return  $this->belongsTo(Model\Provinsi::class);
    }
    
   public function kabupaten(){
    return $this->belongsTo(Model\Kabupaten::class);
   }
   public function kota(){
    return $this->belongsTo(Model\Kota::class);
   }
   public function kecamatan(){
    return $this->belongsTo(Model\Kecamatan::class);
   }
   public function kelurahan(){
    return $this->belongsTo(Model\Kelurahan::class);
   }


   public function cabor(){
    return $this->belongsTo(Model\Cabor::class,'pelatih_id','id');
   }

   public function notif(){
      return $this->hasMany(Model\Notif::class,'user_id','id');
   }

   public function atlitpertandingan(){
    return $this->hasMany(Model\AtlitPertandingan::class);
   }

   public function pengumuman(){
    return $this->hasMany(App\Model\Pengumuman::class);
   }

   public function scopeFilter($query,$request){
      if ($request->has('name')) {
         $query->where('nama','LIKE','%'.$request->get('name').'%');
      }
      if ($request->has('cabor')) {
        $query->where('pelatih_id',$request->get('cabor'));
      }
    }
}
