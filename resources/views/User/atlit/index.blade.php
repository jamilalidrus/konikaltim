@extends('template.user.template-user')

@section("content")

<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Data Atlit</h5>

        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#">Config option 1</a>
            </li>
            <li><a href="#">Config option 2</a>
            </li>
          </ul>
          <a class="close-link">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
      <div>
       
      </div>
          @if(Session::has('message'))
          <div class="alert alert-success alert-dismissable">
            
            <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>
    
          </div>  
          @endif

        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover dataTables-example" >
            <thead>
            <?php  $no=1; ?>
              <tr>

                <th>No</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Username</th>
                <th>Gambar</th>
                <th>Tanggal Lahir</th>
                <th>Alamat</th>                
                <th>No Hp</th>
                <th>Berat Badan</th>
                <th>Pilihan Olahraga</th>                
                <th>Agama</th>
                <th>Jenis Kelamin</th>                                
                

              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              @foreach($user as $usr)
              <tr class="gradeC">
                <td>{{$no++}}</td>
                <td>{{$usr->name}}</td>
                <td>{{$usr->email}}</td>              
                <td>{{$usr->username}}</td>
                <td><a class="btn btn-primary" data-toggle="modal" data-target="#myModal{{$usr->id}}"><i class="fa fa-image"></i> Show</a></td>
                <td>{{$usr->tgl_lahir}}</td>
                <td>{{$usr->kota->nama_kota or 'N/A'}}</td>
               
                <td>{{$usr->no_hp}}</td>
                <td>{{$usr->berat_badan}} Kg</td>
                <td>{{$usr->cabor->nama_cabor}}</td>
               
                <td>{{$usr->agama}}</td>
                <td>@if($usr->kelamin=1)Laki-Laki @else Perempuan @endif</td>
                    
                
              </tr>

              <!-- modal gambar -->
              <div class="modal inmodal fade" id="myModal{{$usr->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title">{{$usr->nama}}</h4>
                          <small class="font-bold"></small>
                        </div>
                        <div class="modal-body">
                          <img class="img img-responsive img-thumbnail" src="{{asset("gambar/user/$usr->gambar")}}">
                        </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
              <!-- endmodal gambar -->
              @endforeach()
            </tbody>
            <tfoot>
              <tr>
                 <th>No</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Username</th>
                <th>Gambar</th>
                <th>Tanggal Lahir</th>
                <th>Alamat</th>                
                <th>No Hp</th>
                <th>Berat Badan</th>
                <th>Pilihan Olahraga</th>                
                <th>Agama</th>
                <th>Status Nikah</th>                                 
               


              </tr>
            </tfoot>
          </table>
          
        </div>
        {!! $user->render() !!}
      </div>
    </div>
  </div>
</div>



      @stop()