@extends('template.user.template-user')

@section("content")
	
	<div class="col-lg-10">
                <div class="contact-box">
                  @if(Session::has('message'))
                  <div class="alert alert-success alert-dismissable">
                       
                       <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>
                       
                  </div>  
                  @endif
                    <a href="#">
                    <div class="col-sm-4">
                        <div class="text-center">
                            <img alt="image"  class="img-circle m-t-xs img-responsive img-responsive img-thumbnail" src="{{asset("gambar/user/$user->gambar")}}">
                            @if(Auth::user()->status == "waiting")
                                 <div class="m-t-xs font-bold">Calon Attlit {{Auth::user()->cabor->nama_cabor}} </div>
                            @else(Auth::user->status == "accepted"))
                                <div class="m-t-xs font-bold">Attlit {{Auth::user()->cabor->nama_cabor}} </div>
                            @endif
                           
                        </div>
                    </div>
                    <div class="col-sm-8">
                    <div align="">

                          <h2 style="float: left;" ><i class="fa fa-user"></i> Profil Anda</h2><a style="float: right;" href="{{route('profile.edit',$user->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit Profil</a> 
                    </div>
                  
                    <table class="table">
                    	<tr>
                    		<td><strong>Nama</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$user->name}}</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Umur</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{date("Y")-substr($user->tgl_lahir,0,4)}} Tahun</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Email</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$user->email}}</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Tanggal Lahir</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$user->tgl_lahir}}</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Kota Asal</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$user->kota->nama_kota}}</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Kota Sekarang</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$user->domisili->nama_kota or '--'}}</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Kontak Ponsel</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$user->no_hp}}</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Berat Badan</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$user->berat_badan}} kg</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Atlit</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$user->cabor->nama_cabor}} </strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Status Atlit</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong><button class="btn btn-primary" >{{$user->status}}</button> </strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Agama</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$user->agama}} </strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Status Kawin</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>@if($user->status_atlit == 0) Belum Menikah @else Sudah Menikah @endif </strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Terdata pada</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$user->created_at->format('d M Y')}} </strong></td>
                    	</tr>
                    </table>
                       
                       
                    </div>
                    <div class="clearfix"></div>
                        </a>
                </div>
            </div>
@stop()