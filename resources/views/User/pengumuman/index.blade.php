 @extends('template.user.template-user')

 @section("content")

 <ul class="notes">
    @foreach($pengumuman as $attention)
  
    @if( in_array(Auth::user()->id,json_decode($attention->tujuan)))
    <li>
        <div>
            <small>{{$attention->created_at}}</small>
            <h5>{{$attention->judul}}</h5>
           
            <a href="{{url("atlit/pengumuman/$attention->id")}}" class="btn btn-sm btn-warning" ><i class="fa fa-eye "></i></a>
        </div>
    </li>
    @else    
    @endif

   @endforeach()
   {!! $pengumuman->render() !!}
</ul>
@stop()