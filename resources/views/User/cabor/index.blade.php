@extends('template.user.template-user')
@section("content")

<div class="row">
	<div class="col-md-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Cabor</h5>

				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#">Config option 1</a>
						</li>
						<li><a href="#">Config option 2</a>
						</li>
					</ul>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
			<div>
				
			</div>
					@if(Session::has('message'))
					<div class="alert alert-success alert-dismissable">
						
						<dt style="font-family:verdana;"><i class="fa fa-check"></i>	{{Session::get('message')}}</dt>
		
					</div>  
					@endif

				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" >
						<thead>
						<?php  $no=1; ?>
							<tr>

								<th>No</th>
								<th>Nama Cabor</th>
								<th>Jumlah Atlit</th>
								

							</tr>
						</thead>
						<tbody>
							<?php $no=1; ?>
							@foreach($cabor as $cbr)
							<tr class="gradeC">
								<td>{{$no++}}</td>
								<td>{{$cbr->nama_cabor}}</td>
								<td>{{App\User::where('olahraga_id',$cbr->id)->get()->count('id')}} Orang</td>
											
								
							</tr>
							@endforeach()
						</tbody>
						<tfoot>
							<tr>
								<th>No</th>
								<th>Nama Cabor</th>
								<th>Jumlah Atlit</th>
								
							</tr>
						</tfoot>
					</table>
					
				</div>
				{!! $cabor->render() !!}
			</div>
		</div>
	</div>
</div>



			@stop()