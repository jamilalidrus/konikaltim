@extends('template.user.template-user')
@section("content")

<div class="row">
	<div class="col-md-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Hai.. {{Auth::user()->name}}</h5>

				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#">Config option 1</a>
						</li>
						<li><a href="#">Config option 2</a>
						</li>
					</ul>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			@foreach($notif as $notifikasi)
			<div class="ibox-content">			
				<div class="table-responsive">
					<div class="col-md-12">
						<i class="fa fa-envelope fa-fw"></i> | {!! str_limit($notifikasi->isi,80, '...') !!}	<a href="{{route('notif.show',$notifikasi->id)}}">read more</a>					
					</div>					
				</div>				
			</div>
			@endforeach()
		</div>
	</div>
</div>



@stop()