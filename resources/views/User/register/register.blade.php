<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from thunder-team.com/friend-finder/index-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 May 2017 16:49:11 GMT -->
<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="This is social network html5 template available in themeforest......" />
		<meta name="keywords" content="Social Network, Social Media, Make Friends, Newsfeed, Profile Page" />
		<meta name="robots" content="index, follow" />
		<title>Friend Finder | A Complete Social Network Template</title>

    <!-- Stylesheets
    ================================================= -->
		<link rel="stylesheet" href="{{asset("finder/css/bootstrap.min.css")}}" />
		<link rel="stylesheet" href="{{asset("finder/css/style.css")}}" />
		<link rel="stylesheet" href="{{asset("finder/css/ionicons.min.css")}}" />
    <link rel="stylesheet" href="{{asset("finder/css/font-awesome.min.css")}}" />
    
    <!--Favicon-->
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
	</head>
	<body>

    <!-- Header
    ================================================= -->
		<header id="header-inverse">
      
    </header>
    <!--Header End-->
    
    <!-- Landing Page Contents
    ================================================= -->
    <div id="lp-register">
    	<div class="container wrapper">
        <div class="row">
        	<div class="col-sm-5">
            <div class="intro-texts">
            	<h1 class="text-white">Make Cool Friends !!!</h1>
            	<p>Friend Finder is a social network template that can be used to connect people. The template offers Landing pages, News Feed, Image/Video Feed, Chat Box, Timeline and lot more. <br /> <br />Why are you waiting for? Buy it now.</p>
              <button class="btn btn-primary">Learn More</button>
            </div>
          </div>
        	<div class="col-sm-6 col-sm-offset-1">
            <div class="reg-form-container"> 
            
              <!-- Register/Login Tabs-->
             
              
              <!--Registration Form Contents-->
              <div class="tab-content">
                <div class="tab-pane active" id="register">
                  <h3>Register Now !!!</h3>
                  <p class="text-muted">Be cool and join today. Meet millions</p>
                  @if(Session::has('message'))
                  <div class="alert alert-danger alert-dismissable">
                    <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>
                  </div>  
                  @endif
                  @if (count($errors) > 0)
                  <div class="alert alert-danger">
                   <h3>Opss.. Something was wrong</h3>
                    <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                  @endif
                  
                  <!--Register Form-->
                {!! Form::open(['route'=>'register','enctype'=>'multipart/form-data','method'=>'post']) !!}
                    <div class="row">
                      <div class="form-group col-xs-6">
                     
                       {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'nama lengkap']) !!}
                      </div>
                      <div class="form-group col-xs-6">
                        {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'email']) !!}
                        
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-xs-12">
                       
                       {!! Form::password('password',['class'=>'form-control','placeholder'=>'password']) !!}
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-xs-12">
                      
                       {!! Form::password('passwordconfirm',['class'=>'form-control','placeholder'=>'reenter password']) !!}
                      </div>
                    </div>
                    <div class="row">
                      <p class="birth"><strong>Date of Birth</strong></p>
                      <div class="form-group col-sm-12 col-xs-6">
                        <label for="month" class="sr-only"></label>
                         {!! Form::date('tgl_lahir',null,['class'=>'form-control','placeholder'=>'tanggal lahir']) !!}
                      </div>
                    </div>
                   <div class="row">
                     <p><strong>Alamat</strong></p>
                      <div class="form-group gender col-xs-12">
                      {!! Form::text('alamat',null,['class'=>'form-control','placeholder'=>'Kota']) !!}
                    </div>
                    <div class="form-group col-xs-12">
                      {!! Form::text('kelurahan',null,['class'=>'form-control','placeholder'=>'Kelurahan']) !!}
                    </div>
                    <div class="form-group col-xs-6">
                      {!! Form::text('jalan',null,['class'=>'form-control','placeholder'=>'Jalan']) !!}
                    </div>
                     <div class="form-group col-xs-6">
                      {!! Form::text('rt',null,['class'=>'form-control','placeholder'=>'RT']) !!}
                    </div>
                    <div class="form-group col-xs-6">
                      {!! Form::text('rw',null,['class'=>'form-control','placeholder'=>'RW']) !!}
                    </div>
                    <div class="form-group col-xs-6">
                      {!! Form::text('no_rumah',null,['class'=>'form-control','placeholder'=>'Nomor Rumah']) !!}
                    </div>
                   </div>
                    <div class="row">
                      <p class="birth"><strong>Profile Picture</strong></p>
                      <div class="form-group col-xs-12">
                      
                     {!! Form::file('picture',['class'=>'form-control']) !!}
                      </div>
                      
                    </div>
                      <p><a href="#">Already have an account?</a></p>
                    <button class="btn btn-primary"  type="submit">Register</button>
                  {!! Form::close() !!}<!--Register Now Form Ends-->
                
                 
                </div><!--Registration Form Contents Ends-->
                
                <!--Login-->
             
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-sm-offset-6">
          
            <!--Social Icons-->
            <ul class="list-inline social-icons">
              <li><a href="#"><i class="icon ion-social-facebook"></i></a></li>
              <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
              <li><a href="#"><i class="icon ion-social-googleplus"></i></a></li>
              <li><a href="#"><i class="icon ion-social-pinterest"></i></a></li>
              <li><a href="#"><i class="icon ion-social-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <!--preloader-->
    

    <!-- Scripts
    ================================================= -->
    <script src="{{asset("finder/js/jquery-3.1.1.min.js")}}"></script>
    <script src="{{asset("finder/js/bootstrap.min.js")}}"></script>
    <script src="{{asset("finder/js/jquery.appear.min.js")}}"></script>
		<script src="{{asset("finder/js/jquery.incremental-counter.js")}}"></script>
    <script src="{{asset("finder/js/script.js")}}"></script>
    
	</body>

<!-- Mirrored from thunder-team.com/friend-finder/index-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 May 2017 16:49:12 GMT -->
</html>
