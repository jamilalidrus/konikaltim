@extends('template.homepage.template-index')
@section("content")

<h2 align="center" > Grafik Jumlah Atlit Berdasarkan Umur </h2>
<hr>
<canvas id="myChart"  style="height: 300px; width: 600px;" ></canvas>
<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            'Muda = 13 - 18 tahun',
            'Remaja = 19 - 25 tahun',
        	'Tua = diatas 30 tahun',
            ],
        datasets: [{
            label: 'Umur    ',
            data: [
                '{{$muda}}',
                '{{$dewasa}}',
            	'{{$tua}}',
                ],
            backgroundColor: [
            	
                    'rgba(146, 218, 22, 1)',                
                    'rgba(45, 190, 45, 1)',                
                	'rgba(38, 140, 38, 1)',                
              
            ],
            borderColor: [
            
                    'rgba(146, 218, 22, 1)',
                    'rgba(45, 190, 45, 1)',
                	'rgba(38, 140, 38, 1)',
              
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

@stop()


