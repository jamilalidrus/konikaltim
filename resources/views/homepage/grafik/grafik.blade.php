@extends('template.homepage.template-index')
@section("content")

<h2 align="center" >Grafik Jumlah Atlit Berdasarkan Cabang Olahraga</h2>
<hr>
<canvas id="myChart" width="100%" height="200"></canvas>
<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels: [
        	 @foreach($cabor as $c)
        		"<?php echo $c->nama_cabor; ?>",
         	@endforeach],
        datasets: [{
            label: 'Jumlah Atlit',
            data: [
            	@foreach($cabor as $c)
            		<?php  echo $c->user->count('name'); ?>,  
             	@endforeach],
            backgroundColor: [
            	@foreach($cabor as $c)
                	'rgba(250, 146, 0, 1)',                
                @endforeach
            ],
            borderColor: [
            	@foreach($cabor as $c)
                	'rgba(255,99,132,1)',
                @endforeach
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

@stop()


