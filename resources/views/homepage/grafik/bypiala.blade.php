@extends('template.homepage.template-index')
@section("content")

<h2 align="center" > Grafik Jumlah Atlit Berdasarkan Medali yang diraih </h2>
<hr>

<canvas id="doughnut-chart" width="70%" height="40"></canvas>
<script>
new Chart(document.getElementById("doughnut-chart"), {
    type: 'doughnut',
    data: {
      labels: [
        @foreach($medali as $c)
            "<?php echo $c->medali ; ?>",
        @endforeach
        ],
      datasets: [
        {
          label: "Jumlah Atlit Permedali",
          backgroundColor: ["#ecc914", "#d7d6d0","#e19d1d"],
          data: [
          @foreach($medali as $c)
            "<?php echo $c->prestasi()->pluck('user_id')->count('id'); ?>",
          @endforeach()
          ]
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Jumlah Atlit Permedali'
      }
    }
});
</script>

@stop()


