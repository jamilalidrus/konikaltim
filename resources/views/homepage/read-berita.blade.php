@extends('template.homepage.template-index')
@section("content")
<style type="text/css">
	.read{
		display: inline-block;
		background-color: rgba(245, 244, 240, 0.63);
		
	}


</style>

<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.10&appId=171038742967598";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<div class="col-md-12 well ">
		<div align="center">
			<h2 >{{$berita->judul}}</h2>
			<br>
			<img src="{{asset("gambar/news/$berita->gambar")}}">
			<hr>
		</div>
		<div>
			{!! $berita->text !!}
		</div>
		<hr>
	</div>

	<div class="">	

		<div class="col-md-12 well" >
			<h2>Tambahkan Komentar anda</h2>
			<div>
				<div class="fb-like" data-href="{{url("news/$berita->id/$berita->url/read")}}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
			</div>
			
			
			<div>
				<div class="fb-comments" data-href="{{url("news/$berita->id/$berita->url/read")}}" data-numposts="5"></div>
			</div>
		</div>
	</div>

@stop()