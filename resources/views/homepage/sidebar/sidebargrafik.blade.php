<div class="col-md-3">
	<div id="back" class="w3_agile_banner_bottom_grid">
		<div class="list-group">
			<a href="{{route('cabor.user')}}" @if(Request::is('grafik/jumlahcatlit')) class="list-group-item active" @else class="list-group-item" @endif >Grafik Atlit Per Cabor</a>
			<a href="{{route('grafik.umur')}}" @if(Request::is('grafik/umur')) class="list-group-item active" @else class="list-group-item" @endif>Grafik Atlit Berdasarkan Usia</a>
			<a href="{{route('grafik.piala')}}"  @if(Request::is('grafik/medali')) class="list-group-item active" @else class="list-group-item" @endif >Grafik Jumlah Atlit Berdasarkan Medali</a>
			<a href="#" class="list-group-item">Grafik Jumlah Regu Berdasarkan Medali</a>
			<a href="{{route('grafik.pelatih')}}" @if(Request::is('grafik/pelatih')) class="list-group-item active" @else class="list-group-item" @endif  >Grafik Jumlah Pelatih Berdasarkan Cabor </a>


		</div>
		
	</div>
</div>
