@extends('template.homepage.template-index')
@section("content")

<style type="text/css">
	.media{
		display: inline-block;
		background-color: rgba(245, 244, 240, 0.63);
		margin-bottom: 0;
	}
	.media-object{
		max-width: 200px;
	}
</style>
	
		<div class="col-md-12">
		@foreach($berita as $news)			
			
			<ul class="media-list">
				<li class="media">
					<div class="media-left">
						<a href="#">
							<img align="left" class="media-object" src="{{asset("gambar/news/$news->gambar")}}">
						</a>
					</div>
					<div class="media-body">
						<h4 class="media-heading"> <a href="{{url("news/$news->id/$news->url/read")}}">{{$news->judul}}</a> </h4>
						<p>{!! str_limit($news->text,200) !!}</p>
						
						<p>Posted: {{$news->created_at->format('Y-M-d')}}</p>
					</div>
				</li>
			</ul>
		@endforeach()
		</div>
		<div class="clear-fix" ></div>
	

@stop()