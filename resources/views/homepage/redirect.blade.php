@extends('template.homepage.template-index')
@section("content")
	<div class="col-md-12">
		@if(Session::has('message'))
          <div class="alert alert-success alert-dismissable">
            
            <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>
    
          </div>  
          @endif

          <div class="col-md-12">
          <div>
            <h1>Silahkan Login Disini</h1>
          </div>
          	
          	<a class="btn btn-warning" href="{{url('atlit/page-login')}}">Login</a>
          </div>
	</div>

@stop()