<div class="movies_nav">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav>
						<ul class="nav navbar-nav">
							<li @if(Request::is('/')) class="active"  @endif ><a href="{{url('/')}}">Home</a>
							</li>
							<li  @if(Request::is('visimisi/sejarah')) class=" dropdown active"  @endif>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tentang Koni <b class="caret"></b></a>
								<ul class="dropdown-menu multi-column columns-2">
									<li>
									<div class="col-sm-6">
										<ul class="multi-column-dropdown">
											<li><a href="{{route('koni.visimisi')}}">Visi Misi dan Sejarah</a></li>
											
										</ul>
									</div>
									
									<div class="clearfix"></div>
									</li>
								</ul>
							</li>
							<li @if(Request::is('allnews')) class="active"  @endif><a href="{{route('all.news')}}">Semua Berita</a></li>
							<li  @if(Request::is('pengumuman')) class="active"  @endif><a href="{{route('all.pengumuman')}}">Pengumuman</a></li>
							<li @if(Request::is('bank')) class="active"  @endif >
								<a href="{{route('bank.data')}}"    >Bank Data <b></b></a>
								<!-- <ul class="dropdown-menu multi-column columns-3">
									<li>
										<div class="col-sm-4">
											<ul class="multi-column-dropdown">
												<li><a href="genres.html">Asia</a></li>
												<li><a href="genres.html">France</a></li>
												<li><a href="genres.html">Taiwan</a></li>
												<li><a href="genres.html">United States</a></li>
											</ul>
										</div>
										<div class="col-sm-4">
											<ul class="multi-column-dropdown">
												<li><a href="genres.html">China</a></li>
												<li><a href="genres.html">HongCong</a></li>
												<li><a href="genres.html">Japan</a></li>
												<li><a href="genres.html">Thailand</a></li>
											</ul>
										</div>
										<div class="col-sm-4">
											<ul class="multi-column-dropdown">
												<li><a href="genres.html">Euro</a></li>
												<li><a href="genres.html">India</a></li>
												<li><a href="genres.html">Korea</a></li>
												<li><a href="genres.html">United Kingdom</a></li>
											</ul>
										</div>
										<div class="clearfix"></div>
									</li>
								</ul> -->
							</li>
							<li @if(Request::is('grafik/*')) class="active"@endif  ><a href="{{route('cabor.user')}}">Statistik</a></li>
							<li  @if(Request::is('home/pendaftaran')) class="active"  @endif ><a href="{{route('pendaftaran')}}">Mendaftar Atlit</a></li>
						</ul>
					</nav>
				</div>
			</nav>	
		</div>
	</div>