@extends('template.homepage.template-index')
@section("content")
<style type="text/css">
	.media{
		display: inline-block;
		background-color: rgba(245, 244, 240, 0.63);
		margin-bottom: 0;
	}
	.media-object{
		max-width: 200px;
	}
</style>
<div class="col-md-12 well ">
	<div align="center">
		<h2 >Semua Berita</h2>
		<br>

		<hr>
	</div>
	<div>
		<div class="col-md-12">
			@foreach($pengumuman as $wanted)			
			
			<ul class="media-list">
				<li class="media">
					<div class="media-left">
						
					</div>
					<div class="media-body">
						<h4 class="media-heading"> <a href="{{route('pengumuman.read',[$wanted->id,$wanted->url])}}">{{$wanted->judul}}</a> </h4>
						<p>{!! str_limit($wanted->isi,200) !!}</p>
						
						<p>Posted: {{$wanted->created_at->format('Y-M-d')}}</p>
					</div>
				</li>
			</ul>
			@endforeach()
		</div>
		<div align="center" >
			{!! $pengumuman->render() !!}
		</div>
		<div class="clear-fix" ></div>
	</div>
</div>

@stop()