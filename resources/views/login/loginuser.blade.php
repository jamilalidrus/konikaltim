@extends('template.login.login-user')
@section("content")
{!! Form::open(['route'=>'user.proses.submit','method'=>'post','class'=>'m-t','role'=>'form']) !!}
	
                <div class="form-group">
                   {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'email']) !!}
                </div>
                <div class="form-group">
                    {!! Form::password('password',['class'=>'form-control','placeholder'=>'password']) !!}
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

              
            

{!! Form::close() !!}
@stop()