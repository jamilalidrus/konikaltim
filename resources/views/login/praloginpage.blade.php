@extends('template.login.pralogin')
@section("content")

<div class="row">
		<div class="col-md-4">
			<a href="{{url('/atlit/page-login')}}" class="btn btn-primary  btn-block" > <i class="fa fa-user" ></i> Atlit</a>
		</div>
		<div class="col-md-4">
			<a href="{{url('/admin/login')}}" class="btn btn-primary  btn-block" > <i class="fa fa-user" ></i> Admin</a>
		</div>
		<div class="col-md-4">
			<a href="{{url('/pelatih/login')}}" class="btn btn-primary  btn-block" > <i class="fa fa-user" ></i> Pelatih</a>
		</div>
	</div>

@stop()