
<fieldset> 
    <div class="row">   
        <div class="col-lg-6">      
           <div class="form-group">
            {!! Form::label('Regu','Regu') !!}
            {!! Form::select('regu_id',[$namaregu],null,['class'=>'chosen-select','tabindex'=>'2'])!!}  
            {!! $errors->first('regu_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}    
            </div> 
        </div>
        <div class="col-lg-6">      
            <div class="form-group">
                {!! Form::label('Medali','Medali') !!}
                {!! Form::select('medali_id',$medali,null,['class'=>'chosen-select','tabindex'=>'2'])!!}  
                {!! $errors->first('medali_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}    
            </div>       
         </div>   


            <div class="col-lg-12">

               {!!Form::submit('Simpan',['class'=>'btn btn-primary']) !!} 
           </div>
       </div>
   </fieldset>



