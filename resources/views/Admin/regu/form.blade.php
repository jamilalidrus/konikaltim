
<fieldset> 
    <div class="row">   
        <div class="col-lg-6">      
           <div class="form-group">
            {!! Form::label('Regu','Regu') !!}
            {!! Form::select('cabor_id',$cabor,null,['class'=>'chosen-select','tabindex'=>'2'])!!}  
            {!! $errors->first('cabor_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}    
            </div> 
        </div>
        <div class="col-lg-6">      
            <div class="form-group">
                {!! Form::label('Kota','Kota') !!}
                {!! Form::select('kota_id',$kota,null,['class'=>'chosen-select','tabindex'=>'2'])!!}  
                {!! $errors->first('kota_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}    
            </div>       
         </div>   


            <div class="col-lg-12">

               {!!Form::submit('Simpan',['class'=>'btn btn-primary']) !!} 
           </div>
       </div>
   </fieldset>



