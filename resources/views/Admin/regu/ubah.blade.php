@extends('template.admin.template-admin')
@section('content')
  
<div class="row">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Ubah Regu</h5>
				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
	

			<div class="ibox-content">
				 

				{!! Form::model($regu,['route'=>['regu.update',$regu->id],'method'=>'put','class'=>'wizard-big']) !!}

					@include('Admin.regu.form')

				{!! Form::close() !!}

			</div>
		</div>
	</div>

</div>


@stop
