@extends('template.admin.template-admin')
@section("content")

<div class="row">
	<div class="col-md-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data pengumuman</h5>

				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#">Config option 1</a>
						</li>
						<li><a href="#">Config option 2</a>
						</li>
					</ul>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
			<div>
			<a href="{{route('pengumumanadmin.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>	Tambah data</a>
			</div>
					@if(Session::has('message'))
					<div class="alert alert-success alert-dismissable">
						
						<dt style="font-family:verdana;"><i class="fa fa-check"></i>	{{Session::get('message')}}</dt>
		
					</div>  
					@endif

				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" >
						<thead>
						<?php  $no=1; ?>
							<tr>
								<th>No</th>
								<th>Judul pengumuman</th>								
								<th>Isi</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							
							@foreach($pengumuman as $skefo)
							<tr class="gradeC">
								<td>{{$loop->iteration}}</td>
								<td>{{$skefo->judul}}</td>							 
								<td><a class="btn btn-primary" data-toggle="modal" data-target="#myModal2{{$skefo->id}}"><i class="fa fa-tasks"></i> Show</a></td>
								<td>
									{!! Form::open(['route'=>['pengumumanadmin.destroy',$skefo->id],'method'=>'delete','onsubmit'=>'return confirm("Yakin Ingin Menghapus?")'])!!}
									<a class="btn btn-warning" href="{{route('pengumumanadmin.edit',$skefo->id)}}"><i class="fa fa-pencil"></i>	Ubah</a>
									<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>	Hapus</button>
									{!! Form::close()!!}
								</td>				
								
							</tr>
			
            
               <!-- modal text -->
              <div class="modal inmodal fade" id="myModal2{{$skefo->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        
                          <small class="font-bold"></small>
                        </div>
                        <div class="modal-body">
                          {!! $skefo->isi !!}
                        </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
              <!-- endmodal text-->
              		
              		
					@endforeach()
						</tbody>
						<tfoot>
							<tr>
								<th>No</th>
								<th>Judul pengumuman</th>								
								<th>Isi</th>
								<th>Aksi</th>
							</tr>
						</tfoot>
					</table>
					
				</div>
				{!! $pengumuman->render() !!}
			</div>
		</div>
	</div>
</div>



			@stop()