@extends('template.admin.template-admin')
@section('content')
 

	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Tambah Data pengumuman</h5>
				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
		
			<div class="ibox-content">
				 

				{!! Form::open(['route'=>'pengumumanadmin.store','method'=>'post','class'=>'wizard-big','enctype=multipart/form-data']) !!}

					@include('Admin.pengumuman.form')

				{!! Form::close() !!}

			</div>
		</div>
	</div>



@stop
