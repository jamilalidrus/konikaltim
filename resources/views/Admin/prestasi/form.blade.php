
<fieldset> 
    <div class="row">   
        <div class="col-lg-12">      
            <div class="form-group">
                {!! Form::label('Nama Atlit','Nama Atlit') !!}
                {!! Form::select('user_id',$user,null,['class'=>'chosen-select','tabindex'=>'2']) !!}
                {!! $errors->first('user_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}  
            </div>            
            
         </div>
         <div class="col-lg-6">      
            <div class="form-group">
                {!! Form::label('Nama Cabor','Nama Cabor') !!}
                {!! Form::select('cabor_id',$cabor,null,['class'=>'chosen-select','tabindex'=>'2']) !!}
                {!! $errors->first('cabor_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}  
            </div>            
            
         </div>
          <div class="col-lg-6">      
            <div class="form-group">
                {!! Form::label('Jenis Medali','Jenis Medali') !!}
                {!! Form::select('piala_id',$medali,null,['class'=>'chosen-select','tabindex'=>'2']) !!}
                {!! $errors->first('piala_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}  
            </div>            
            
         </div>
         
         <div class="col-lg-12">
            
             {!!Form::submit('Simpan',['class'=>'btn btn-primary']) !!} 
         </div>
     </div>
 </fieldset>



