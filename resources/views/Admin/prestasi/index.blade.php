@extends('template.admin.template-admin')

@section("content")

<div class="row">
	<div class="col-md-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Prestasi Atlit</h5>

				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#">Config option 1</a>
						</li>
						<li><a href="#">Config option 2</a>
						</li>
					</ul>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
				<div class="row">
					{!! Form::open(['url'=>'/admin/prestasi','method'=>'get']) !!}
					
					<div class="col-md-3" >
						{!! Form::select('cabor',$cabor,null,['class'=>'form-control chosen-select','placeholder'=>'Cari Cabor','tabindex'=>'2']) !!}
					</div>
					<div class="col-md-3" >
						{!! Form::select('medali',$medali,null,['class'=>'form-control chosen-select','placeholder'=>'Cari Medali','tabindex'=>'2']) !!}
					</div>
					<div class="col-md-2">
						<button class="btn btn-primary" >Cari</button>
					</div>
					{!! Form::close() !!}
					<div class="col-md-4 " align="right">
						<a href="{{route('prestasi.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>	Tambah data</a>
					</div>
					

				</div>
				<br>
					@if(Session::has('message'))
					<div class="alert alert-success alert-dismissable">
						
						<dt style="font-family:verdana;"><i class="fa fa-check"></i>	{{Session::get('message')}}</dt>
		
					</div>  
					@endif

				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" >
						<thead>
						<?php  $no=1; ?>
							<tr>

								<th>No</th>
								<th>Nama Atlit</th>
								<th>Nama Cabor</th>											
								<th>Jenis Medali</th>											
								<th>Aksi</th>

							</tr>
						</thead>
						<tbody>
							<?php $no=1; ?>
							@foreach($prestasi as $pres)
							<tr class="gradeC">
								<td>{{$no++}}</td>
								<td>{{$pres->user->name or 'N/A'}}</td>
								<td>{{$pres->cabor->nama_cabor}}</td>
								<td>{{$pres->piala->medali}}</td>
								<td>
									{!! Form::open(['route'=>['prestasi.destroy',$pres->id],'method'=>'delete','onsubmit'=>'return confirm("Yakin Ingin Menghapus?")'])!!}
									<a class="btn btn-warning" href="{{route('prestasi.edit',$pres->id)}}"><i class="fa fa-pencil"></i>	Ubah</a>
									<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>	Hapus</button>
									{!! Form::close()!!}
								</td>				
								
							</tr>
							@endforeach()
						</tbody>
						<tfoot>
							<tr>
								<th>No</th>
								<th>Nama Atlit</th>
								<th>Nama Cabor</th>											
								<th>Jenis Medali</th>											
								<th>Aksi</th>


							</tr>
						</tfoot>
					</table>
					
				</div>
				{!! $prestasi->appends(Request::only('medali','cabor'))->links() !!}
			</div>
		</div>
	</div>
</div>



			@stop()