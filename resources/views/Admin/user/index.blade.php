@extends('template.admin.template-admin')

@section("content")

<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Data Atlit</h5>

        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#">Config option 1</a>
            </li>
            <li><a href="#">Config option 2</a>
            </li>
          </ul>
          <a class="close-link">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
        <div class="row">
           {!! Form::open(['url'=>'/admin/atlit','method'=>'get']) !!}
          <div class="col-md-3" >
              {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Cari atlit']) !!}
          </div>
          <div class="col-md-3" >
              {!! Form::select('cabor',$cabor,null,['class'=>'form-control chosen-select','placeholder'=>'Cari Cabor','tabindex'=>'2']) !!}
          </div>
          <div class="col-md-2">
            <button class="btn btn-primary" >Cari</button>
          </div>
        {!! Form::close() !!}
         <div class="col-md-4" align="right" >
           <a href="{{url('admin/atlit/create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>  Tambah data</a>
        </div>
        </div>
       
        <br>
     
          @if(Session::has('message'))
          <div class="alert alert-success alert-dismissable">
            
            <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>
    
          </div>  
          @endif

        <div>
          <table class="table table-striped table-bordered table-hover dataTables-example" id="atlit-table" >
            <thead>
          
              <tr>

                <th>No</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Email</th>
                <th>Tanggal Lahir</th>
                <th>Gambar</th>
                <th>Jenis Kelamin</th>
                <th>Nama Ayah</th>
                <th>Nama Ibu</th>
                <th>No Hp</th>
               <th>Berat Badan</th>
                <th>Cabor</th>
                <th>Status</th>
                <th>Agama</th>
                <th>Status</th>
                <th>Tinggi Badan</th>
                <th>Darah</th>
                <th>Provinsi</th>
                <th>Kabupaten</th>
                <th>Kota</th>
                <th>Kecamatan</th>
                <th>Kelurahan</th>
                <th>Sekolah</th>
                <th>Kelas</th>
                <th>Alamat Sekolah</th>
                <th>Sepatu</th>
                <th>Kaos</th>
                <th>Kameja</th>
                <th>Hobi</th>
                <th>Action</th>
                
               
              </tr>
            </thead>
            <tbody>
             <?php $no=1; ?>
              @foreach($user as $usr)
              <tr class="gradeC">
                <td>{{$no++}}</td>
                <td>{{$usr->name}}</td>
                <td>{{$usr->username}}</td>              
                <td>{{$usr->email}}</td>
                <td>{{$usr->tgl_lahir}}</td>
                <td><a class="btn btn-primary" data-toggle="modal" data-target="#myModal{{$usr->id}}"><i class="fa fa-image"></i> Show</a></td>
                <td>@if($usr->kelamin == 1)Laki - Laki @else Perempuan @endif</td>
                <td>{{$usr->ayah}}</td>
                <td>{{$usr->ibu}}</td>
                <td>{{$usr->no_hp}}</td>
                <td>{{$usr->berat_badan}}</td>
                <td>{{$usr->cabor->nama_cabor or 'N/A'}}</td>
                <td>@if($usr->status_atlit == 1)Nikah @else Belum Menikah @endif</td>
                <td>{{$usr->agama}}</td>
                <td>{{$usr->status}}</td>
                <td>{{$usr->tinggi}}</td>
                <td>{{$usr->darah}}</td>
                <td>{{$usr->provinsi->nama_provinsi or 'Belum Ada'}}</td>
                <td>{{$usr->kabupaten->nama_kabupaten or 'Belum Ada'}}</td>
                <td>{{$usr->kota->nama_kota or 'Belum Ada'}}</td>
                <td>{{$usr->kecamatan->nama_kecamatan or 'Belum Ada'}}</td>
                <td>{{$usr->kelurahan->nama_kelurahan or 'Belum Ada'}}</td>
                <td>{{$usr->sekolah}}</td>
                <td>{{$usr->kelas}}</td>
                <td>{{$usr->alamat_sekolah}}</td>
                <td>{{$usr->sepatu}}</td>
                <td>{{$usr->kaos}}</td>
                <td>{{$usr->kameja}}</td>
                <td>{{$usr->hobi}}</td>
                <td>
                  {!! Form::open(['route'=>['atlit.destroy',$usr->id],'method'=>'delete','onsubmit'=>'return confirm("Yakin Ingin Menghapus?")'])!!}
                  <a class="btn btn-warning" href="{{route('atlit.edit',$usr->id)}}"><i class="fa fa-pencil"></i> Ubah</a>
                  <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>  Hapus</button>
                  {!! Form::close()!!}
                </td>       
                
              </tr>

              <!-- modal gambar -->
              <div class="modal inmodal fade" id="myModal{{$usr->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title">{{$usr->nama}}</h4>
                          <small class="font-bold"></small>
                        </div>
                        <div class="modal-body">
                          <img class="img img-responsive img-thumbnail" src="{{asset("gambar/user/$usr->gambar")}}">
                        </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
              <!-- endmodal gambar -->
              @endforeach()
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Email</th>
                <th>Tanggal Lahir</th>
                <th>Gambar</th>
                <th>Jenis Kelamin</th>
                <th>Nama Ayah</th>
                <th>Nama Ibu</th>
                <th>No Hp</th>
               <th>Berat Badan</th>
                <th>Cabor</th>
                <th>Status</th>
                <th>Agama</th>
                <th>Status</th>
                <th>Tinggi Badan</th>
                <th>Darah</th>
                <th>Provinsi</th>
                <th>Kabupaten</th>
                <th>Kota</th>
                <th>Kecamatan</th>
                <th>Kelurahan</th>
                <th>Sekolah</th>
                <th>Kelas</th>
                <th>Alamat Sekolah</th>
                <th>Sepatu</th>
                <th>Kaos</th>
                <th>Kameja</th>
                <th>Hobi</th>
                <th>Action</th>
               
                



              </tr>
            </tfoot>
          </table>
          
        </div>
        {!! $user->appends(Request::only('name','cabor'))->links() !!}
      </div>
    </div>
  </div>
</div>


@stop()

@push('scripts')
<!-- <script>
$(function() {
    $('#atlit-table').DataTable({
        processing: true,
        serverSide: true,
        responsive:true,
        pageLength: 10,
        
        ajax: '{!! route('atlit.data') !!}',
        columns: [
        {data:'number', name: 'number' },
        {data:'name',name: 'name'},
        {data:'username',name:'username'},
        {data:'email',name:'email'},
       { data:'gambar',name:'gambar',orderable: false, searchable: false},
        {data:'tgl_lahir',name:'tgl_lahir'},
        {data:'cabang',name:'cabang',searchable: false},
        {data:'kelamin',name:'kelamin'},
        {data:'ayah',name:'ayah'},
        {data:'ibu',name:'ibu'},
        {data:'no_hp',name:'no_hp'},
        {data:'berat_badan',name:'berat_badan'},
        { data:'agama',name:'agama'},
        { data:'status',name:'status'},
        { data:'tinggi',name:'tinggi'},
        { data:'darah',name:'darah'},
        { data:'sekolah',name:'sekolah'},
        { data:'kelas',name:'kelas'},
        { data:'alamat_sekolah',name:'alamat_sekolah'},
        { data:'sepatu',name:'sepatu'},
        { data:'kaos',name:'kaos'},
        { data:'kameja',name:'kameja'},
        { data:'hobi',name:'hobi'},
        { data:'action',name:'action',orderable: false, searchable: false},
       

           
        ]
    });

});

</script>
 -->

@endpush