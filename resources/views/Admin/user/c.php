 <?php $no=1; ?>
              @foreach($user as $usr)
              <tr class="gradeC">
                <td>{{$no++}}</td>
                <td>{{$usr->name}}</td>
                <td>{{$usr->username}}</td>              
                <td>{{$usr->email}}</td>
                <td>{{$usr->tgl_lahir}}</td>
                <td><a class="btn btn-primary" data-toggle="modal" data-target="#myModal{{$usr->id}}"><i class="fa fa-image"></i> Show</a></td>
                <td>{{$usr->kelamin}}</td>
                <td>{{$usr->ayah}}</td>
                <td>{{$usr->ibu}}</td>
                <td>{{$usr->no_hp}}</td>
                <td>{{$usr->berat_badan}}</td>
                <td>{{$usr->cabor->nama_cabor or 'N/A'}}</td>
                <td>{{$usr->status_atlit}}</td>
                <td>{{$usr->agama}}</td>
                <td>{{$usr->status}}</td>
                <td>{{$usr->tinggi}}</td>
                <td>{{$usr->darah}}</td>
                <td>{{$usr->provinsi->nama_provinsi or 'Belum Ada'}}</td>
                <td>{{$usr->kabupaten->nama_kabupaten or 'Belum Ada'}}</td>
                <td>{{$usr->kota->nama_kota or 'Belum Ada'}}</td>
                <td>{{$usr->kecamatan->nama_kecamatan or 'Belum Ada'}}</td>
                <td>{{$usr->kelurahan->nama_kelurahan or 'Belum Ada'}}</td>
                <td>{{$usr->sekolah}}</td>
                <td>{{$usr->kelas}}</td>
                <td>{{$usr->alamat_sekolah}}</td>
                <td>{{$usr->sepatu}}</td>
                <td>{{$usr->kaos}}</td>
                <td>{{$usr->kameja}}</td>
                <td>{{$usr->hobi}}</td>
                <td>
                  {!! Form::open(['route'=>['atlit.destroy',$usr->id],'method'=>'delete','onsubmit'=>'return confirm("Yakin Ingin Menghapus?")'])!!}
                  <a class="btn btn-warning" href="{{route('atlit.edit',$usr->id)}}"><i class="fa fa-pencil"></i> Ubah</a>
                  <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>  Hapus</button>
                  {!! Form::close()!!}
                </td>       
                
              </tr>

              <!-- modal gambar -->
              <div class="modal inmodal fade" id="myModal{{$usr->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title">{{$usr->nama}}</h4>
                          <small class="font-bold"></small>
                        </div>
                        <div class="modal-body">
                          <img class="img img-responsive img-thumbnail" src="{{asset("gambar/user/$usr->gambar")}}">
                        </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
              <!-- endmodal gambar -->
              @endforeach()