<fieldset> 
	@if ($errors->any())
	<div class="col-md-12">
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>

	@endif
    <div class="col-md-12">
		<h2 align="center" >Data Pribadi</h2>
		<hr>
		<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('Nama Lengkap','Nama Lengkap') !!}
				{!! Form::text('name',null,['class'=>'form-control']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('Email','Email') !!}
				{!! Form::email('email',null,['class'=>'form-control']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('Password','Password') !!}
				{!! Form::password('password',['class'=>'form-control']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('Reenter Password','Reenter Password') !!}
				{!! Form::password('repassword',['class'=>'form-control']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('Nama Ayah','Nama Ayah') !!}
				{!! Form::text('ayah',null,['class'=>'form-control']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('Nama Ibu','Nama Ibu') !!}
				{!! Form::text('ibu',null,['class'=>'form-control']) !!}					
			</div>
			
			<div class="form-group">
				{!! Form::label('Foto','Foto') !!}
				{!! Form::file('gambar',['class'=>'form-control']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('Tanggal Lahir','Tanggal Lahir') !!}
				{!! Form::date('tgl_lahir',null,['class'=>'form-control']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('Jenis Kelamin','Jenis Kelamin') !!}
				{!! Form::select('kelamin',['1'=>'Laki-Laki','0'=>'Perempuan'],null,['class'=>'form-control']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('No Hp','No Hp') !!}
				{!! Form::number('no_hp',null,['class'=>'form-control']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('Status','Status') !!}
				{!! Form::select('status_atlit',['1'=>'Nikah','0'=>'Belum Nikah'],null,['class'=>'form-control']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('Agama','Agama') !!}
				{!! Form::select('agama',['islam'=>'Islam','katolik'=>'Katolik','protestan'=>'Protestan','hindu'=>'Hindu','budha'=>'Budha','konghucu'=>'Konghucu'],null,['class'=>'form-control']) !!}					
			</div>
			
			
		</div>
		<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('Olahraga Pilihan','Olahraga Pilihan') !!}
				{!! Form::select('olahraga_id',$cabor,null,['class'=>'form-control chosen-select','tabindex'=>'2']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('Asal Sekolah PendidikanTerakhir','Asal Sekolah PendidikanTerakhir') !!}
				{!! Form::text('sekolah',null,['class'=>'form-control']) !!}					
			</div>	
			<div class="form-group">
				{!! Form::label('Kelas','Kelas') !!}
				{!! Form::text('kelas',null,['class'=>'form-control']) !!}					
			</div>	
			<div class="form-group">
				{!! Form::label('Alamat Sekolah','Alamat Sekolah') !!}
				{!! Form::text('alamat_sekolah',null,['class'=>'form-control']) !!}					
			</div>
			<div class="form-group">
				{!! Form::label('Ukuran Sepatu','Ukuran Sepatu') !!}
				{!! Form::number('sepatu',null,['class'=>'form-control']) !!}					
			</div>	
			<div class="form-group">
				{!! Form::label('Ukuran Kaos','Ukuran Kaos') !!}
				{!! Form::select('kaos',['S'=>'S','M'=>'M','L'=>'L','XL'=>'XL','XXL'=>'XXL'],null,['class'=>'form-control']) !!}					
			</div>	
			<div class="form-group">
				{!! Form::label('Ukuran Kameja','Ukuran Kameja') !!}
				{!! Form::select('kameja',['S'=>'S','M'=>'M','L'=>'L','XL'=>'XL','XXL'=>'XXL'],null,['class'=>'form-control']) !!}					
			</div>	
			<div class="form-group">
				{!! Form::label('Hobi','Hobi') !!}
				{!! Form::text('hobi',null,['class'=>'form-control']) !!}					
			</div>	
			
			<div class="form-group">
				{!! Form::label('Berat Badan','Berat Badan') !!}
				{!! Form::text('berat_badan',null,['class'=>'form-control']) !!}					
			</div>
			
			<div class="form-group">
				{!! Form::label('Tinggi Badan','Tinggi Badan') !!}
				{!! Form::number('tinggi',null,['class'=>'form-control']) !!}		
			</div>
			<div class="form-group">
				{!! Form::label('Golongan Darah','Golongan Darah') !!}
				{!! Form::text('darah',null,['class'=>'form-control']) !!}		
			</div>
			
		</div>
	</div>
	
	<hr>
	<div class="col-md-12">

		<h2 align="center" >Alamat Kelahiran</h2>
		<hr>
		<div class="col-md-6">
			<div class="form-group">			
				{!! Form::label('Provinsi','Provinsi') !!}
				{!! Form::select('provinsi_id',$provinsi,null,['class'=>'form-control','id'=>'sProvinsi'])!!}
			</div>
			<div class="form-group">			
				{!! Form::label('Kabupaten','Kabupaten') !!}
				{!! Form::select('kabupaten_id',array(),null,['class'=>'form-control','id'=>'sKabupaten'])!!}
			</div>
			<div class="form-group">			
				{!! Form::label('Kota','Kota') !!}
				{!! Form::select('kota_id',array(),null,['class'=>'form-control','id'=>'sKota'])!!}	
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">			
				{!! Form::label('Kecamatan','Kecamatan') !!}
				{!! Form::select('kecamatan_id',array(),null,['class'=>'form-control','id'=>'sKecamatan'])!!}			
			</div>
			<div class="form-group">			
				{!! Form::label('Kelurahan','Kelurahan') !!}
				{!! Form::select('kelurahan_id',array(),null,['class'=>'form-control','id'=>'sKelurahan'])!!}	
			</div>
		</div>
	</div>
	<div class="col-md-12">	
		<h2 align="center">Kota Domisili Sekarang</h2>
		<hr>
		<div class="col-md-12">
			<div class="form-group">
				{!! Form::label('Kota Domisili Calon Atlit','Kota Domisili Calon Atlit') !!}
				{!! Form::select('kota_domisili',$kabkota,null,['class'=>'form-control']) !!}	
				<p style="color: red;" >*harap isi berdasarkan kota domisili anda sekarang</p>				
			</div>
		</div>	
		

	</div>
	
	<div class="col-md-12">
		<button  class="btn btn-primary btn-block" type="submit" >Simpan</button>
	</div>
	</fieldset>
