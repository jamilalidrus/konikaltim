@extends('template.admin.template-admin')

@section("content")

<div class="row">
	<div class="col-md-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data pertandingan</h5>

				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#">Config option 1</a>
						</li>
						<li><a href="#">Config option 2</a>
						</li>
					</ul>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
			<div>
				<a href="{{route('pertandingan.create')}}" class="btn btn-primary">Tambah</a>
			</div>
					@if(Session::has('message'))
					<div class="alert alert-success alert-dismissable">						
						<dt style="font-family:verdana;"><i class="fa fa-check"></i>	{{Session::get('message')}}</dt>
		
					</div>  
					@endif

				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" >
						<thead>
						<?php  $no=1; ?>
							<tr>

								<th>No</th>
								<th>Nama Pertandingan</th>
								<th>Tahun</th>
								<th>Status</th>
								<th>Aksi</th>

							</tr>
						</thead>
						<tbody>
							
							@forelse($pertandingan as $pr)
							<tr class="gradeC">
								<td>{{$loop->iteration}}</td>
								<td>{{$pr->nama}}</td>
								<td>{{$pr->tahun->tahun or 'N/A'}}</td>
								<td> <a onclick="return Confirm('Yakin?')" href="{{route('pertandingan.show',$pr->id)}}"  @if($pr->status == 'nonactive') class="btn btn-danger" @else class="btn btn-primary" @endif >{{$pr->status}}</a> </td>
								<td>
									{!! Form::open(['route'=>['pertandingan.destroy',$pr->id],'method'=>'delete','onsubmit'=>'return confirm("Yakin Ingin Menghapus?")'])!!}
									<a class="btn btn-warning" href="{{route('pertandingan.edit',$pr->id)}}"><i class="fa fa-pencil"></i>	Ubah</a>
									<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>	Hapus</button>
									{!! Form::close()!!}
								</td>				
								
							</tr>

							
              		@empty
              		
					@endforelse()
						</tbody>
						<tfoot>
							<tr>
								<th>No</th>
								<th>Nama Pertandingan</th>
								<th>Tahun</th>
								<th>Status</th>
								<th>Aksi</th>
						</tfoot>
					</table>
					
				</div>

			</div>
		</div>
	</div>
</div>



			@stop()