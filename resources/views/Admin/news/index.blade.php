@extends('template.admin.maintemplate')
@section("content")

	<div class="x_panel">
                  <div class="x_title">
                    <h2>Data Berita <small>News</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div>
                    	<a class="btn btn-success" href="{{route('news.create')}}"><i class="fa fa-plus"></i>	Tambah Data</a>
                    </div>
                    @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">

                      <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>

                    </div>  
                    @endif
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                      <?php $no=1; ?>
                        <tr>
                          <th>No</th>
                          <th>Judul</th>                          
                          <th>Picture</th>
                          <td>Isi</td>
                          <td>Admin</td>                                          
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                      @foreach($news as $new)
                        <tr>
                        
                          <td>{{$no++}}</td>
                          <td>{{$new->judul}}</td>                          
                          <td><a class="btn btn-success" data-toggle="modal" data-target="#myModalImage{{$new->id}}"><i class="fa fa-image"></i></a></td>
                          <td><a href="{{route('news.show',$new->id)}}">link isi disini</a></td>
                          <td>{{$new->admin->name or 'N/A'}}</td>
                        
                          <td>


                          {!! Form::open(['route'=>['news.destroy',$new->id],'method'=>'delete','onsubmit'=>'return confirm("Yakin Ingin Menghapus?")'])!!}
                          <a class="btn btn-warning" href="{{route('news.edit',$new->id)}}"><i class="fa fa-pencil"></i>  Edit</a> 
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>  Hapus</button>
                        {!! Form::close()!!}

                          </td>
                          
                           <!-- Modal gambar -->
                      <div id="myModalImage{{$new->id}}" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-sm">
                        

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">{{$new->nama}}</h4>
                            </div>
                            <div class="modal-body">
                             
                                  <img class="img img-responsive img-thumbnail" src="{{asset("gambar/news/$new->gambar")}}">
                                
                              
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        
                        </div>
                      </div>
                       
                        </tr>
                       @endforeach() 
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
@endsection()