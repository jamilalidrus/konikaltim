@extends('template.admin.maintemplate')
@section("content")

<div class="x_panel">
  <div class="x_title">
    <h2>Tambah Data Berita <small>News</small></h2>

    <div class="clearfix"></div>
  </div>
  <div class="x_content">
   @if (count($errors) > 0)
                  <div class="alert alert-danger">
                   <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                  @endif
  {!! Form::open(['route'=>'news.store','method'=>'post','enctype'=>'multipart/form-data']) !!}
   @include('Admin.news.form')
   {!! Form::close() !!}

 </div>
</div>
</div>

@endsection()