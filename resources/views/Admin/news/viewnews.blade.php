<style type="text/css">
  #pp{
    width: 100px;
    border-radius: 50%;
     }
      #map {
       
        width: 100%;
        height: 400px;  
      
      }
</style>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbfCeebcn4mGH0pfEK5p6IgSQt9hpEw6M&callback=initMap">
    </script>
@extends('template.admin.maintemplate')
@section("content")

  <div class="x_panel">
                  <div class="x_title">
                    <h2>View News <small>Activity report</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="{{asset("gambar/news/$news->gambar")}}" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3>{{$news->judul}}</h3>

                      <ul class="list-unstyled user_data">
                       

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i> Dikirim pada {{$news->created_at}}
                        </li>

                        
                      </ul>

                      <a href="{{route('news.edit',$news->id)}}" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit News</a>
                      <br />

                      <!-- start skills -->
                     
                      <!-- end of skills -->

                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
                          <h2>Kiriman {{$news->admin->name}}</h2>
                        </div>
                        <div class="col-md-6">
                          <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                            <span>{{$news->created_at}}</span> <b class="caret"></b>
                          </div>
                        </div>
                      </div>
                      <!-- start of user-activity-graph -->
                      <hr>
                      <!-- end of user-activity-graph -->

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <div class="well">
                          <p align="center"><h3 align="center">{{$news->judul}}</h3></p>
                          <hr>
                          <p>{!! $news->isi !!}</p>
                          </div>
                        </div>
                       
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                            <!-- start recent activity -->
                            <ul class="messages">
                             @if(Session::has('message'))
                             <div class="alert alert-success alert-dismissable">

                              <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>

                            </div>  
                            @endif
                            @forelse($comment as $komentar)
                              <li>
                                <img src="{{asset("gambar/user/{$komentar->user->picture}")}}" class="avatar" alt="Avatar">
                                <div class="message_date">
                                  {!! Form::open(['route'=>['newscomment.destroy',$komentar->id],'method'=>'delete','onsubmit'=>'return confirm("Yakin Ingin Menghapus?")']) !!}
                                    <button class="btn btn-sm btn-danger" ><i class="fa fa-trash"></i></button>
                                  {!! Form::close() !!}
                                  <p class="month"></p>
                                </div>
                                <div class="message_wrapper">
                                 <a href="{{route('user.show',$komentar->user->id)}}">  <h4 class="heading">{{$komentar->user->name}}</h4> </a>
                                  <blockquote class="message">{{$komentar->isi}}</blockquote>
                                  <br />
                                  <p class="url">
                                    <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                    <a href="#"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i> {{$komentar->created_at}} </a>
                                  </p>
                                </div>
                              </li>
                              @empty
                              @endforelse()
                              {!! $comment->render() !!}
                            </ul>
                            <!-- end recent activity -->

                          </div>
                        
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui
                              photo booth letterpress, commodo enim craft beer mlkshk </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                
        @stop()

       