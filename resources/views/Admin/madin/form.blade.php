
<fieldset> 
    <div class="row"> 
        <div class="col-lg-12">
             <div class="form-group">
                {!! Form::label('Judul','Judul') !!}
                {!! Form::text('judul',null,['class'=>'form-control'])!!}  
                {!! $errors->first('judul', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}    
            </div>      
        </div>         
        <div class="col-lg-10">      
            <div class="form-group">
            {!! Form::label('Isi','Isi') !!}
            {!! Form::textarea('isi',null,['class'=>'fr-view'])!!}  
            {!! $errors->first('isi', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}    
        </div>       
            
         </div>
         
         <div class="col-lg-12">
            
             {!!Form::submit('Simpan',['class'=>'btn btn-primary']) !!} 
         </div>
     </div>
 </fieldset>



