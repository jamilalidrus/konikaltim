
<fieldset> 
    <div class="row">   
        <div class="col-lg-6">      
            <div class="form-group">
            {!! Form::label('Pertandingan','Pertandingan') !!}
            {!! Form::text('nama',null,['class'=>'form-control'])!!}  
            {!! $errors->first('nama', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}    
            </div>     
        </div>
        <div class="col-lg-6">      
            <div class="form-group">
            {!! Form::label('Tahun Pelaksanaan','Tahun Pelaksanaan') !!}
            {!! Form::select('tahun_id',$tahun,null,['class'=>'form-control'])!!}  
            {!! $errors->first('tahun_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}    
            </div>     
        </div>
         
         <div class="col-lg-12">
            
             {!!Form::submit('Simpan',['class'=>'btn btn-primary']) !!} 
         </div>
     </div>
 </fieldset>



