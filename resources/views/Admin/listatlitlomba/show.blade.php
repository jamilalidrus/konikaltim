@extends('template.admin.template-admin')

@section("content")

<div class="row">
	<div class="col-md-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data list lomba {{$pertandingan->nama}} ({{$pertandingan->tahun->tahun}}) </h5>

				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#">Config option 1</a>
						</li>
						<li><a href="#">Config option 2</a>
						</li>
					</ul>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
			
					@if(Session::has('message'))
					<div class="alert alert-success alert-dismissable">						
						<dt style="font-family:verdana;"><i class="fa fa-check"></i>	{{Session::get('message')}}</dt>
		
					</div>  
					@endif

				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" >
						<thead>
						<?php  $no=1; ?>
							<tr>

								<th>No</th>
								<th>Nama Atlit</th>
								<th>Asal</th>
								<th>Cabor</th>
								<th>Pelatih</th>
								<th>Action</th>
								
							</tr>
						</thead>
						<tbody>
							
							@forelse($atlitpertandingan as $list)
							<tr class="gradeC">
								<td>{{$loop->iteration}}</td>
								<td>{{$list->user->name or 'N/A'}}</td>
								<td>{{$list->user->kabupaten->nama_kabupaten or 'N/A'}}</td>
								<td>{{$list->cabor->nama_cabor or 'N/A'}}</td>
								<td>{{$list->pelatih->nama or 'N/A'}}</td>
								<td>
									{!! Form::open(['route'=>['listatlitlomba.destroy',$list->id],'method'=>'delete','onsubmit'=>'return confirm("Yakin ingin membatalkan peserta ini?")']) !!}
									<button class="btn btn-danger" >Reject</button>
									{!! Form::close() !!}
								</td>
							
																		
											
								
							</tr>

							
              		@empty
              		
					@endforelse()
						</tbody>
						<tfoot>
							<tr>
								<th>No</th>
								<th>Nama Atlit</th>
								<th>Asal</th>
								<th>Cabor</th>
								<th>Pelatih</th>
								<th>Action</th>
						</tfoot>
					</table>
					
				</div>
				{!! $atlitpertandingan->render() !!}
			</div>
		</div>
	</div>
</div>



			@stop()