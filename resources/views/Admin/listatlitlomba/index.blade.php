@extends('template.admin.template-admin')

@section("content")

<div class="row">
	<div class="col-md-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data list lomba </h5>

				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#">Config option 1</a>
						</li>
						<li><a href="#">Config option 2</a>
						</li>
					</ul>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
			
					@if(Session::has('message'))
					<div class="alert alert-success alert-dismissable">						
						<dt style="font-family:verdana;"><i class="fa fa-check"></i>	{{Session::get('message')}}</dt>
		
					</div>  
					@endif

				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" >
						<thead>
						<?php  $no=1; ?>
							<tr>

								<th>No</th>
								<th>Nama Pertandingan</th>
								<th>Periode</th>
								<th>Pendaftar</th>
								
							</tr>
						</thead>
						<tbody>
							
							@forelse($pertandingan as $list)
							<tr class="gradeC">
								<td>{{$loop->iteration}}</td>
								<td>{{$list->nama}}</td>
								<td>{{$list->tahun->tahun}}</td>
								<td><a class="btn btn-primary" href="{{route('listatlitlomba.show',$list->id)}}">Lihat Pendaftar</a></td>											
											
								
							</tr>

							
              		@empty
              		
					@endforelse()
						</tbody>
						<tfoot>
							<tr>
								<th>No</th>
								<th>Nama Atlit</th>
								<th>Periode</th>
								<th>Pendaftar</th>
						</tfoot>
					</table>
					
				</div>

			</div>
		</div>
	</div>
</div>



			@stop()