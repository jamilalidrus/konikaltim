@extends('template.admin.template-admin')

@section("content")

<div class="row">
	<div class="col-md-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Cabor Prestasi</h5>

				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#">Config option 1</a>
						</li>
						<li><a href="#">Config option 2</a>
						</li>
					</ul>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
			<div>
				<a href="{{route('caborprestasi.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>	Tambah data</a>
			</div>
					@if(Session::has('message'))
					<div class="alert alert-success alert-dismissable">
						
						<dt style="font-family:verdana;"><i class="fa fa-check"></i>	{{Session::get('message')}}</dt>
		
					</div>  
					@endif

				<div class="table-responsive">
					
					<table id="example" class="table table-striped table-bordered table-hover " cellspacing="0" width="100%">
        <thead>
            <tr>
            	<th>No</th>
                <th>Cabor</th>
                <th>Emas</th>
                <th>Perak</th>
                <th>Perunggu</th>
                <th>Jumlah</th>
                <th>Aksi</th>
            </tr>
            <?php $no=1; ?>
        </thead>
        <tfoot>
            <tr>
                <th class="success" >Total:</th>
                <th class="success" colspan="6" style="text-align:right">Total:</th>
                
            </tr>
        </tfoot>
        <tbody>

        	@foreach($cabprestasi as $cp)
        	<tr class="gradeC">
        		<td>{{$no++}}</td>
        		<td>{{$cp->cabor->nama_cabor}}</td>
        		<td>{{$cp->cabor->prestasi->count()}} Medali</td>
        		<td>{{$cp->perak or '0'}} Medali</td>
        		<td>{{$cp->perunggu or '0'}} Medali</td>
        		<td>{{$cp->cabor->prestasi->count()+$cp->perak+$cp->perunggu}}</td>
        		<td>
        			{!! Form::open(['route'=>['caborprestasi.destroy',$cp->id],'method'=>'delete','onsubmit'=>'return confirm("Yakin Ingin Menghapus?")'])!!}
        			<a class="btn btn-warning" href="{{route('caborprestasi.edit',$cp->id)}}"><i class="fa fa-pencil"></i>	Ubah</a>
        			<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>	Hapus</button>
        			{!! Form::close()!!}
        		</td>	


        	</tr>

        	@endforeach()

        </tbody>
    </table>
					
				</div>

			</div>
		</div>
	</div>
</div>



			@stop()