@extends('template.admin.template-admin')
@section('content')
  
<div class="row">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Ubah Data Cabor Prestasi</h5>
				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
	

			<div class="ibox-content">
				 

				{!! Form::model($cabprestasi,['route'=>['caborprestasi.update',$cabprestasi->id],'method'=>'put','class'=>'wizard-big']) !!}

					@include('Admin.caborprestasi.form')

				{!! Form::close() !!}

			</div>
		</div>
	</div>

</div>


@stop
