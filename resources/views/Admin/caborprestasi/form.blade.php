
<fieldset> 
    <div class="row">   
        <div class="col-lg-12">      
            <div class="form-group">
                {!! Form::label('Nama Cabor','Nama Cabor') !!}
                {!! Form::select('cabor_id',$cabor,null,['class'=>'form-control']) !!}
                {!! $errors->first('cabor_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}  
            </div>            
            
         </div>
         <div class="col-lg-6">      
            <div class="form-group">
                {!! Form::label('Jumlah Medali Perak','Jumlah Medali Perak') !!}
                {!! Form::text('perak',null,['class'=>'form-control']) !!}
                {!! $errors->first('perak', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}  
            </div>            
            
         </div>

         <div class="col-lg-6">      
            <div class="form-group">
                {!! Form::label('Jumlah Medali Perunggu','Jumlah Medali Perunggu') !!}
                {!! Form::text('perunggu',null,['class'=>'form-control']) !!}
                {!! $errors->first('perunggu', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}  
            </div>            
            
         </div>         
         <div class="col-lg-12">
            
             {!!Form::submit('Simpan',['class'=>'btn btn-primary']) !!} 
         </div>
     </div>
 </fieldset>



