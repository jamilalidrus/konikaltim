@extends('template.admin.template-admin')

@section("content")

<div class="row">
	<div class="col-md-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Cabor</h5>

				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#">Config option 1</a>
						</li>
						<li><a href="#">Config option 2</a>
						</li>
					</ul>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
			<div>
				<a href="{{route('cabor.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>	Tambah data</a>
			</div>
					@if(Session::has('message'))
					<div class="alert alert-success alert-dismissable">
						
						<dt style="font-family:verdana;"><i class="fa fa-check"></i>	{{Session::get('message')}}</dt>
		
					</div>  
					@endif

				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="users-table" >
						<thead>
					
							<tr>

								<th>No</th>
								<th>Nama Cabor</th>
								<th>Action</th>
							

							</tr>
						</thead>
						
						<tfoot>
							<tr>
								<th>No</th>
								<th>Nama Cabor</th>
								<th>Action</th>
								
							</tr>
						</tfoot>
					</table>
					
				</div>
				
			</div>
		</div>
	</div>
</div>
@stop()

@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.data') !!}',
        columns: [
            { data: 'number', name: 'number' },
            { data: 'nama_cabor', name: 'nama_cabor' },
            {data: 'action', name: 'action',orderable: false, searchable: false},
           
        ]
    });
});
</script>
@endpush