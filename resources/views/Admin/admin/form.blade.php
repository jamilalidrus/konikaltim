<fieldset>
	<div class="row">
		<div class="col-md-6">
	<div class="form-group">			
				{!! Form::label('Name','Name') !!}
				{!! Form::text('name',null,['class'=>'form-control'])!!}	
				{!! $errors->first('name', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		
		<div class="form-group">			
				{!! Form::label('Password','Password') !!}
				{!! Form::password('password',['class'=>'form-control'])!!}	
				{!! $errors->first('password', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		<div class="form-group">			
				{!! Form::label('Password','Password') !!}
				{!! Form::password('password_confirm',['class'=>'form-control'])!!}	
				{!! $errors->first('password_confirm', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		
		
	</div>
	<div class="col-md-6">
		<div class="form-group">			
				{!! Form::label('Gambar','Gambar') !!}
				{!! Form::file('gambar',['class'=>'form-control'])!!}	
				{!! $errors->first('picture', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		<div class="form-group">			
				{!! Form::label('Email','Email') !!}
				{!! Form::email('email',null,['class'=>'form-control'])!!}	
				{!! $errors->first('email', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		
	</div>
	<div class="col-md-12">
		<button class="btn btn-primary" type="submit">Simpan</button>
	</div>
	</div>
</fieldset>
	




