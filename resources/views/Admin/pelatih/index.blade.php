@extends('template.admin.template-admin')

@section("content")

<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Data Pelatih</h5>

        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#">Config option 1</a>
            </li>
            <li><a href="#">Config option 2</a>
            </li>
          </ul>
          <a class="close-link">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
         <div class="row">
           {!! Form::open(['url'=>'/admin/pelatih','method'=>'get']) !!}
          <div class="col-md-3" >
              {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Cari Pelatih']) !!}
          </div>
          <div class="col-md-3" >
              {!! Form::select('cabor',$cabor,null,['class'=>'form-control chosen-select','placeholder'=>'Cari Cabor','tabindex'=>'2']) !!}
          </div>
          <div class="col-md-2">
            <button class="btn btn-primary" >Cari</button>
          </div>
        {!! Form::close() !!}
          <div class="col-md-4 " align="right">
        <a href="{{url('admin/pelatih/create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>  Tambah data</a>
      </div>
        </div>
    
          @if(Session::has('message'))
          <div class="alert alert-success alert-dismissable">
            
            <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>
    
          </div>  
          @endif

        <div>
          <table class="table table-striped table-bordered table-hover dataTables-example" >
            <thead>
            <?php  $no=1; ?>
              <tr>

                <th>No</th>
                <th>Nama</th>              
                <th>email</th>
                <th>Tanggal Lahir</th>
                <th>Gambar</th>
                <th>Jenis Kelamin</th>                
                <th>No Hp</th>                
                <th>Pelatih Cabor</th>               
                <th>Agama</th>               
                <th>Provinsi</th>
                <th>Kabupaten</th>
                <th>Kota</th>
                <th>Kecamatan</th>
                <th>Kelurahan</th>
                <th>Rincian Alamat</th>
                <th>Aksi</th>

              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              @foreach($pelatih as $plt)
              <tr class="gradeC">
                <td>{{$no++}}</td>
                <td>{{$plt->nama}}</td>                            
                <td>{{$plt->email}}</td>
                <td>{{$plt->tgl_lahir}}</td>
                <td><a class="btn btn-primary" data-toggle="modal" data-target="#myModal{{$plt->id}}"><i class="fa fa-image"></i> Show</a></td>
                <td>
                  @if($plt->kelamin == 1)
                    Laki-Laki
                  @elseif($plt->kelamin == 0)
                    Perempuan
                  @endif
                 
                </td>
                <td>{{$plt->no_hp}}</td>
               
                <td>{{$plt->cabor->nama_cabor}}</td>             
                <td>{{$plt->agama}}</td>            
                <td>{{$plt->provinsi->nama_provinsi or 'Belum Ada'}}</td>
                <td>{{$plt->kabupaten->nama_kabupaten or 'Belum Ada'}}</td>
                <td>{{$plt->kota->nama_kota or 'Belum Ada'}}</td>
                <td>{{$plt->kecamatan->nama_kecamatan or 'Belum Ada'}}</td>
                <td>{{$plt->kelurahan->nama_kelurahan or 'Belum Ada'}}</td>
                <td>{{$plt->alamat_rinci}}</td>               
                <td>
                  {!! Form::open(['route'=>['pelatih.destroy',$plt->id],'method'=>'delete','onsubmit'=>'return confirm("Yakin Ingin Menghapus?")'])!!}
                  <a class="btn btn-warning" href="{{route('pelatih.edit',$plt->id)}}"><i class="fa fa-pencil"></i> Ubah</a>
                  <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>  Hapus</button>
                  {!! Form::close()!!}
                </td>       
                
              </tr>

              <!-- modal gambar -->
              <div class="modal inmodal fade" id="myModal{{$plt->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title">{{$plt->nama}}</h4>
                          <small class="font-bold"></small>
                        </div>
                        <div class="modal-body">
                          <img class="img img-responsive img-thumbnail" src="{{asset("gambar/pelatih/$plt->gambar")}}">
                        </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
              <!-- endmodal gambar -->
              @endforeach()
            </tbody>
            <tfoot>
              <tr>
               <th>No</th>
                <th>Nama</th>              
                <th>email</th>
                <th>Tanggal Lahir</th>
                <th>Gambar</th>
                <th>Jenis Kelamin</th>                
                <th>No Hp</th>                
                <th>Pelatih Cabor</th>               
                <th>Agama</th>               
                <th>Provinsi</th>
                <th>Kabupaten</th>
                <th>Kota</th>
                <th>Kecamatan</th>
                <th>Kelurahan</th>
                <th>Rincian Alamat</th>
                <th>Aksi</th>



              </tr>
            </tfoot>
          </table>
          
        </div>
    {!! $pelatih->appends(Request::only('name','cabor'))->links() !!}
      </div>
    </div>
  </div>
</div>



      @stop()