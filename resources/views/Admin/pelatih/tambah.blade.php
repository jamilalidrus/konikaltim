@extends('template.admin.template-admin')
@section('content')
 

  <div class="col-lg-12">
    <div class="ibox">
      <div class="ibox-title">
        <h5>Tambah Data Pelatih</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
          </a>
          
          <a class="close-link">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
    
      <div class="ibox-content">
         @if(Session::has('message'))
          <div class="alert alert-danger alert-dismissable">
            
            <dt style="font-family:verdana;"><i class="fa fa-times"></i>  {{Session::get('message')}}</dt>
    
          </div>  
          @endif 

        {!! Form::open(['route'=>'pelatih.store','method'=>'post','class'=>'wizard-big','enctype'=>'multipart/form-data']) !!}

          @include('Admin.pelatih.form')

        {!! Form::close() !!}

      </div>
    </div>
  </div>



@stop
