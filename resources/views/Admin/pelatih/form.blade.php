<fieldset> 
    <div class="row">  
	<div class="col-md-6">
	<div class="form-group">			
				{!! Form::label('Name','Name') !!}
				{!! Form::text('nama',null,['class'=>'form-control'])!!}	
				{!! $errors->first('nama', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		<div class="form-group">			
				{!! Form::label('Jenis Kelamin','Jenis Kelamin') !!}
				{!! Form::select('kelamin',['laki-laki'=>'Laki-Laki','perempuan'=>'Perempuan'],null,['class'=>'form-control'])!!}	
				{!! $errors->first('kelamin', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		<div class="form-group">			
				{!! Form::label('Email','Email') !!}
				{!! Form::email('email',null,['class'=>'form-control'])!!}	
				{!! $errors->first('email', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		
		<div class="form-group">			
				{!! Form::label('Password','Password') !!}
				{!! Form::password('password',['class'=>'form-control'])!!}	
				{!! $errors->first('password', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		<div class="form-group">			
				{!! Form::label('Gambar','Gambar') !!}
				{!! Form::file('gambar',['class'=>'form-control'])!!}	
				{!! $errors->first('gambar', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		
		
	</div>
	<div class="col-md-6">
		
		
		<div class="form-group">			
				{!! Form::label('TGL Lahir','TGL Lahir') !!}
				{!! Form::date('tgl_lahir',null,['class'=>'form-control'])!!}	
				{!! $errors->first('tgl_lahir', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		
		<div class="form-group">			
				{!! Form::label('Nomor HP','Nomor HP') !!}
				{!! Form::text('no_hp',null,['class'=>'form-control'])!!}	
				{!! $errors->first('no_hp', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		
		
	
		
		<div class="form-group">			
				{!! Form::label('Bidang Cabor','Bidang Cabor') !!}
				{!! Form::select('pelatih_id',$cabor,null,['class'=>'form-control chosen-select','tabindex'=>'2'])!!}	
				{!! $errors->first('pelatih_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
	
		<div class="form-group">			
				{!! Form::label('Agama','Agama') !!}
				{!! Form::select('agama',['Islam'=>'Islam','Protestan'=>'Protestan','Katolik'=>'Katolik','Budha'=>'Budha','Konghucu'=>'Konghucu'],null,['class'=>'form-control'])!!}	
				{!! $errors->first('agama', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>

	
		
		
		
	</div>
	
	<div class="col-md-12">
	<hr>
		<h2>Alamat</h2>
	</div>
	
	<div class="col-md-6">
		<div class="form-group">			
				{!! Form::label('Provinsi','Provinsi') !!}
				{!! Form::select('provinsi_id',$provinsi,null,['class'=>'form-control','id'=>'sProvinsi'])!!}	
				{!! $errors->first('provinsi_id','<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		<div class="form-group">			
				{!! Form::label('Kabupaten','Kabupaten') !!}
				{!! Form::select('kabupaten_id',array(),null,['class'=>'form-control','id'=>'sKabupaten'])!!}	
				{!! $errors->first('kabupaten_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		<div class="form-group">			
				{!! Form::label('Kota','Kota') !!}
				{!! Form::select('kota_id',array(),null,['class'=>'form-control','id'=>'sKota'])!!}	
				{!! $errors->first('kota_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		
	</div>
	<div class="col-md-6">
	 <div class="form-group">			
				{!! Form::label('Kecamatan','Kecamatan') !!}
				{!! Form::select('kecamatan_id',array(),null,['class'=>'form-control','id'=>'sKecamatan'])!!}	
				{!! $errors->first('kecamatan_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		<div class="form-group">			
				{!! Form::label('Kelurahan','Kelurahan') !!}
				{!! Form::select('kelurahan_id',array(),null,['class'=>'form-control','id'=>'sKelurahan'])!!}	
				{!! $errors->first('kelurahan_id', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
		<div class="form-group">			
				{!! Form::label('Tambahan','Tambahan') !!}
				{!! Form::text('alamat_rinci',null,['class'=>'form-control']) !!}
				{!! $errors->first('alamat_rinci', '<dt style="font-family:verdana;color:red;"><i class="fa fa-times"></i>:message</dt>') !!}	
		</div>
	</div>
	<div class="col-md-12">
		<button class="btn btn-primary" type="submit">Simpan</button>
	</div>
	</div>
	</fieldset>
