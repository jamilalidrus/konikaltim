<style type="text/css">
  #pp{
    width: 100px;
    border-radius: 50%;
     }
      #map {
       
        width: 100%;
        height: 400px;  
      
      }
</style>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbfCeebcn4mGH0pfEK5p6IgSQt9hpEw6M&callback=initMap">
    </script>
@extends('template.admin.maintemplate')
@section("content")

  <div class="x_panel">
                  <div class="x_title">
                    <h2>User Profil <small>Activity report</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="{{asset("gambar/user/{$user->picture}")}}" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3>{{$user->name}}</h3>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i> {{$user->alamat}}
                        </li>

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i> pengguna sejak {{$user->created_at}}
                        </li>

                        
                      </ul>

                      <a href="{{route('user.edit',$user->id)}}" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                      <br />

                      <!-- start skills -->
                     
                      <!-- end of skills -->

                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
                          <h2>{{$user->name}}</h2>
                        </div>
                      </div>
                       <hr>
                      <div>
                      <table class="table">
                        <tr>
                          <td>Nama</td>
                          <td>:</td>
                          <td>{{$user->name}}</td>
                        </tr>

                        <tr>
                          <td>Username</td>
                          <td>:</td>
                          <td>{{$user->username}}</td>
                        </tr>
                        <tr>
                          <td>Email</td>
                          <td>:</td>
                          <td>{{$user->email}}</td>
                        </tr>
                        <tr>
                          <td>Tanggal Lahir</td>
                          <td>:</td>
                          <td>{{$user->tgl_lahir}}</td>
                        </tr>
                        <tr>
                          <td>Alamat</td>
                          <td>:</td>
                          <td>{{$user->alamat}}</td>
                        </tr>
                        <tr>
                          <td>Member Sejak</td>
                          <td>:</td>
                          <td>{{$user->created_at}}</td>
                        </tr>
                      </table>
                        
                      </div>
                      <!-- start of user-activity-graph -->
                     
                      <!-- end of user-activity-graph -->

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                       
                       
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                            <!-- start recent activity -->
                            <ul class="messages">
                             @if(Session::has('message'))
                             <div class="alert alert-success alert-dismissable">

                              <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>

                            </div>  
                            @endif
                           
                             
                            </ul>
                            <!-- end recent activity -->

                          </div>
                        
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui
                              photo booth letterpress, commodo enim craft beer mlkshk </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>


  <div class="x_panel">
                  <div class="x_title">
                    <h2>Kiriman {{$user->name}} <small>Activity report</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    

                      <!-- start skills -->
                     
                      <!-- end of skills -->

                    </div>
                    <div class="col-md-12 col-sm-9 col-xs-12">
                      <div>
                      <table id="datatable" class="table table-bordered">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Lokasi</th>
                            <th>Kiriman</th>                            
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; ?>
                        @forelse($post as $feed)
                          <tr>
                            <td>{{$no++}}</td>
                            <td>{{$feed->lokasi}}</td>
                            <td>{{$feed->saran}}</td>
                            <td>
                            {!! Form::open(['route'=>['post.destroy',$feed->id],'method'=>'delete','onsubmit'=>'return confirm("apakah anda yakin ingin menghapus?")']) !!}
                            <a class="btn btn-sm btn-primary" href="{{route('post.show',$feed->id)}}">View</a>
                            <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>  delete</button>

                            </td>
                          </tr>
                        @empty
                        <h2>Belum ada kiriman</h2>
                        @endforelse()
                        </tbody>
                      </table>
                        
                      </div>
                      {!! $post->render() !!}
                      <!-- start of user-activity-graph -->
                     
                      <!-- end of user-activity-graph -->

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                       
                       
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                            <!-- start recent activity -->
                           
                            <!-- end recent activity -->

                          </div>
                        
                         </div>
                      </div>
                    </div>
                  </div>
                
        @stop()

       