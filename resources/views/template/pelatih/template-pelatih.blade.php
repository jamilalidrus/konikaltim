<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.6/table_data_tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Sep 2016 11:23:56 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Pelatih</title>

    <link href="{{asset("admin/css/bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{asset("admin/font-awesome/css/font-awesome.css")}}" rel="stylesheet">

    <link href="{{asset("admin/css/plugins/dataTables/datatables.min.css")}}" rel="stylesheet">

    <link href="{{asset("admin/css/animate.css")}}" rel="stylesheet">
    <link href="{{asset("admin/css/style.css")}}" rel="stylesheet">
      <link href="{{asset("admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css")}}" rel="stylesheet">
      <link href="css/plugins/iCheck/custom.css" rel="stylesheet">

     <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
      <!-- Include Editor style. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript">

            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                });
            }, 6000);
        </script>

</head>

<body>
<?php $info=Auth::user(); ?>
    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img style="width: 50%;" alt="image" class="img-circle img-responsive img-thumbnail" src="{{asset("gambar/pelatih/$info->gambar")}}" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{Auth::user()->nama}}</strong>
                             </span> <span class="text-muted text-xs block">Pelatih {{$info->cabor->nama_cabor or ''}} <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="{{route('profile.index')}}">Profile</a></li>
                            
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
               
                <li>
                    <a href="{{url('pelatih/landing')}}"><i class="fa fa-home"></i> <span class="nav-label">Home</span></a>
                </li>         
             
              
                <li>
                    <a href="#"><i class="fa fa-tasks"></i> <span class="nav-label">Semua Atlit </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{url('pelatih/allatlit')}}">Lihat Data</a></li>                     
                       
                    </ul>
                </li>          
            
                <li>
                    <a href="#"><i class="fa fa-group"></i> <span class="nav-label">Atlit Binaan</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{url('pelatih/atlitbinaan')}}">Lihat Data</a></li>
                      
                    </ul>
                </li> 
                 <li>
                    <a href="#"><i class="fa fa-group"></i> <span class="nav-label">Calon Atlit</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{url('pelatih/calonatlit')}}">Lihat Data</a></li>
                      
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-group"></i> <span class="nav-label">Pengumuman</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{url('pelatih/pengumuman')}}">Lihat Pengumuman</a></li>
                        <li><a href="{{url('pelatih/pengumuman/create')}}">Buat Pengumuman</a></li>
                      
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-group"></i> <span class="nav-label">Pertandingan</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                    @foreach(App\Model\Pertandingan::where('status','active')->get() as $data)
                        <li><a href="{{url("pelatih/selectatlit/$data->id")}}">{{$data->nama}} ({{$data->tahun->tahun or 'N/A'}})</a></li>
                    @endforeach()                      
                      
                    </ul>
                </li>
                 <li>
                    <a href="#"><i class="fa fa-group"></i> <span class="nav-label">Atlit Pertandingan</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{url('pelatih/atlitpertandingan')}}">Lihat Atlit Terdaftar</a></li>
                      
                    </ul>
                </li>                
          
               
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="http://webapplayers.com/inspinia_admin-v2.6/search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to Koni Kaltim Information System</span>
                </li>
               
               

                <li>
                    <a href="{{route('pelatih.logout')}}">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
           
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    @section("content")
                    @show()
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2017
            </div>
        </div>

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="{{asset("admin/js/jquery-2.1.1.js")}}"></script>
    <script src="{{asset("admin/js/bootstrap.min.js")}}"></script>
    <script src="{{asset("admin/js/plugins/metisMenu/jquery.metisMenu.js")}}"></script>
    <script src="{{asset("admin/js/plugins/slimscroll/jquery.slimscroll.min.js")}}"></script>

    <script src="{{asset("admin/js/plugins/dataTables/datatables.min.js")}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset("admin/js/inspinia.js")}}"></script>
    <script src="{{asset("admin/js/plugins/pace/pace.min.js")}}"></script>

     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>

    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/js/froala_editor.pkgd.min.js"></script>

     <script> $(function() { $('textarea').froalaEditor() }); </script>
  

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
    <script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable( {
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 5 ).footer() ).html(
                'Medali '+pageTotal +' ( Total '+ total +' Medali )'
            );
        }
    } );
} );
</script>

</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.6/table_data_tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Sep 2016 11:23:58 GMT -->
</html>
