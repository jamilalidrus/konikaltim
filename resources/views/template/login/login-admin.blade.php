<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.6/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Sep 2016 11:15:25 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>ADMIN | Login</title>

    <link href="{{asset("admin/css/bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{asset("admin/font-awesome/css/font-awesome.css")}}" rel="stylesheet">

    <link href="{{asset("admin/css/animate.css")}}" rel="stylesheet">
    <link href="{{asset("admin/css/style.css")}}" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 style="font-size: 100px;" >KONI</h1>
                <p>Admin</p>

            </div>
            @if(Session::has('message'))
            <div class="alert alert-danger alert-dismissable">
                <dt style="font-family:verdana;"><i class="fa fa-times"></i>    {{Session::get('message')}}</dt>
            </div>
            @endif
                @section("content")
                @show()
            <p class="m-t"> <small>Koni Kaltim</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{asset("admin/js/jquery-2.1.1.js")}}"></script>
    <script src="{{asset("admin/js/bootstrap.min.js")}}"></script>

</body>

<!-- Mirrored from webapplayers.com/inspinia_admin-v2.6/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Sep 2016 11:15:25 GMT -->
</html>
