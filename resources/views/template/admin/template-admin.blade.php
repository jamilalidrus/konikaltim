<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.6/table_data_tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Sep 2016 11:23:56 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KONI|Admin</title>

    <link href="{{asset("admin/css/bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{asset("admin/font-awesome/css/font-awesome.css")}}" rel="stylesheet">
   <!--  <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> -->

    <link href="{{asset("admin/css/plugins/dataTables/datatables.min.css")}}" rel="stylesheet">
    <link href="{{asset("admin/css/plugins/chosen/bootstrap-chosen.css")}}" rel="stylesheet">
      
    <link href="{{asset("admin/css/animate.css")}}" rel="stylesheet">
    <link href="{{asset("admin/css/style.css")}}" rel="stylesheet">
    

     <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
      <!-- Include Editor style. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript">

            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                });
            }, 6000);
        </script>

</head>

<body>
<?php $info=Auth::user(); ?>
    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img style="width: 50%;" alt="image" class="img-circle img-responsive img-thumbnail" src="{{asset("gambar/admin/$info->gambar")}}" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{Auth::user()->name}}</strong>
                             </span> <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="mailbox.html">Mailbox</a></li>
                            <li class="divider"></li>
                            <li><a href="login.html">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                
                <li>
                    <a href="{{route('homepage.index')}}"><i class="fa fa-home"></i> <span class="nav-label">Home</span></a>
                </li>
                @if(Auth::guard('admin')->user())
                <li >
                    <a href=""><i class="fa fa-user"></i> <span class="nav-label">Admin</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('admin.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('admin.create')}}">Tambah Data</a></li>
                       
                    </ul>
                </li>
                @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-group"></i> <span class="nav-label">Atlit </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('atlit.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('atlit.create')}}">Tambah Data</a></li>
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-group"></i> <span class="nav-label">Pelatih </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('pelatih.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('pelatih.create')}}">Tambah Data</a></li>
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Berita </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('berita.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('berita.create')}}">Tambah Data</a></li>
                       
                    </ul>
                </li>
               @endif
               @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Cabor </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('cabor.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('cabor.create')}}">Tambah Data</a></li>
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Regu </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('regu.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('regu.create')}}">Tambah Data</a></li>
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li >
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Medali </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('medali.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('medali.create')}}">Tambah Data</a></li>
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Prestasi Atlit </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('prestasi.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('prestasi.create')}}">Tambah Data</a></li>
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Prestasi Regu </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('reguprestasi.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('reguprestasi.create')}}">Tambah Data</a></li>
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Prestasi Cabor </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('caborprestasi.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('caborprestasi.create')}}">Tambah Data</a></li>
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Visi Misi </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('visimisi.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('visimisi.create')}}">Tambah Data</a></li>
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Sejarah </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('sejarah.index')}}">Lihat Data</a></li>
                     
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Pertandingan </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('pertandingan.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('pertandingan.create')}}">Tambah Data</a></li>
                        <li><a href="{{route('tahun.index')}}">Tahun</a></li>
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Atlit Pertandingan </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('listatlitlomba.index')}}">Lihat Data</a></li>
                        
                       
                    </ul>
                </li>
               @endif
                @if(Auth::guard('admin')->user())
                <li>
                    <a href="mailbox.html"><i class="fa fa-tasks"></i> <span class="nav-label">Pengumuman </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('pengumumanadmin.index')}}">Lihat Data</a></li>
                        <li><a href="{{route('pengumumanadmin.create')}}">Tambah Data</a></li>
                        
                       
                    </ul>
                </li>
               @endif
               
                
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="http://webapplayers.com/inspinia_admin-v2.6/search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Koni Kaltim</span>
                </li>
              <!--   <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a7.jpg">
                                </a>
                                <div class="media-body">
                                    <small class="pull-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li> -->
                       <!--  <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a4.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right text-navy">5h ago</small>
                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/profile.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right">23h ago</small>
                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                </div>
                            </div>
                        </li> -->
                       <!--  <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="mailbox.html">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li> -->
                  <!--   </ul>
                </li> -->
              <!--   <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="mailbox.html">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile.html">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="grid_options.html">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li> -->


                <li>
                    <a href="{{route('admin.logout')}}">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
           
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    @section("content")
                    @show()
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2017
            </div>
        </div>

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="{{asset("admin/js/jquery-2.1.1.js")}}"></script>
    <script src="{{asset("admin/js/bootstrap.min.js")}}"></script>
    <script src="{{asset("admin/js/plugins/metisMenu/jquery.metisMenu.js")}}"></script>
    <script src="{{asset("admin/js/plugins/slimscroll/jquery.slimscroll.min.js")}}"></script>
   <!--   <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->

    <script src="{{asset("admin/js/plugins/dataTables/datatables.min.js")}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset("admin/js/inspinia.js")}}"></script>
    <script src="{{asset("admin/js/plugins/pace/pace.min.js")}}"></script>
    <script src="{{asset("admin/js/plugins/chosen/chosen.jquery.js")}}"></script>
  

     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>

    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/js/froala_editor.pkgd.min.js"></script>
     <script> $(function() { $('textarea').froalaEditor() }); </script>
  

    <!-- Page-Level Scripts -->
    <script type="text/javascript">
    $('#sProvinsi').on('change', function(){
        $.post('{{url('drop') }}', {_token:'{{Session::token()}}', type: 'kabupaten_id', id: $('#sProvinsi').val()}, function(e){
            $('#sKabupaten').html(e);
        });

        $('#sKota').html('');
        $('#sKecamatan').html('');
        $('#sKelurahan').html('');
       
    });
    $('#sKabupaten').on('change', function(){
        $.post('{{ url('drop') }}',{_token:'{{Session::token()}}', type: 'kota_id', id: $('#sKabupaten').val()}, function(e){
            $('#sKota').html(e);
        });
        
        $('#sKecamatan').html('');
        $('#sKelurahan').html('');
    });
     $('#sKota').on('change', function(){
        $.post('{{ url('drop') }}',{_token:'{{Session::token()}}', type: 'kecamatan_id', id: $('#sKota').val()}, function(e){
            $('#sKecamatan').html(e);
        });
        
         
        $('#sKelurahan').html('');
    });
     $('#sKecamatan').on('change', function(){
        $.post('{{ url('drop') }}',{_token:'{{Session::token()}}', type: 'kelurahan_id', id: $('#sKecamatan').val()}, function(e){
            $('#sKelurahan').html(e);
        });
         
       
    });
     
</script>



   <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                paginate: false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>

   
 <script>
       
        $('.chosen-select').chosen({width: "100%"});

    </script>
@stack('scripts')
</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.6/table_data_tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Sep 2016 11:23:58 GMT -->
</html>
