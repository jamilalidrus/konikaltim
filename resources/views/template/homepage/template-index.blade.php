<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from p.w3layouts.com/demos/aug-2016/24-08-2016/one_movies/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 11 Jun 2017 09:20:46 GMT -->
<head>
<title>Koni Kaltim</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="One Movies Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

@if(Request::is('news/*'))
<meta property="og:url"           content="https://developers.facebook.com/docs/plugins/" />

<meta property="og:type"          content="website" />
<meta property="og:title"         content="{{$berita->judul}}" />
<meta property="og:description"   content="Your description" />
<meta property="og:image"         content="{{asset("gambar/news/$berita->gambar")}}" />
@endif

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="{{asset("homepage/css/bootstrap.css")}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{asset("homepage/css/style.css")}}" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="{{asset("homepage/css/contactstyle.css")}}" type="text/css" media="all" />
<link rel="stylesheet" href="{{asset("homepage/css/faqstyle.css")}}" type="text/css" media="all" />
<link href="{{asset("homepage/css/single.css")}}" rel='stylesheet' type='text/css' />
<link href="{{asset("homepage/css/medile.css")}}" rel='stylesheet' type='text/css' />
<!-- banner-slider -->
<link href="{{asset("homepage/css/jquery.slidey.min.css")}}" rel="stylesheet">
<!-- //banner-slider -->
<!-- pop-up -->
<link href="{{asset("homepage/css/popuo-box.css")}}" rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up -->
<!-- font-awesome icons -->
<link rel="stylesheet" href="{{asset("homepage/css/font-awesome.min.css")}}" />
<!-- //font-awesome icons -->
<!-- js -->
<script type="text/javascript" src="{{asset("homepage/js/jquery-2.1.4.min.js")}}"></script>
<!-- //js -->
<!-- banner-bottom-plugin -->
<link href="{{asset("homepage/css/owl.carousel.css")}}" rel="stylesheet" type="text/css" media="all">
<script src="{{asset("homepage/js/owl.carousel.js")}}"></script>
<script>
	$(document).ready(function() { 
		$("#owl-demo").owlCarousel({
	 
		  autoPlay: 3000, //Set AutoPlay to 3 seconds
	 
		  items : 5,
		  itemsDesktop : [640,4],
		  itemsDesktopSmall : [414,3]
	 
		});
	 
	}); 
</script> 

<script type="text/javascript" src="{{asset("js/Chart.js")}}" ></script>
<!-- //banner-bottom-plugin -->
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{asset("homepage/js/move-top.js")}}"></script>
<script type="text/javascript" src="{{asset("homepage/js/easing.js")}}"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	


<script src='../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js')}}"></script>



<link rel="stylesheet" type="text/css" href="{{asset("homepage/custom/style")}}">
<style type="text/css">
	#back{
		background-color: rgba(245, 244, 240, 0.63);
	}
</style>

<body>
<!-- header -->
	<div class="header">
		<div class="">
			<div class="col-md-12">
				<div align="center" class="col-md-8"><img align="left" width="55%" class="img img-responsive" src="{{asset("gambar/koni.png")}}"> </div>
				<!-- <div class="col-md-5">
			
					
				</div> -->
				<div class="col-md-4">
					<div style="float: right;" class="w3_search">
				<form action="#" method="post">
					<input type="text" name="Search" placeholder="Search" required="">
					<input type="submit" value="Go">
				</form>
			</div>
				</div>
			</div>
			
			
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->
<!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Sign In & Sign Up
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				
			</div>
		</div>
	</div>
	
<!-- //bootstrap-pop-up -->
<!-- nav -->
	@include('homepage.head.head')
<!-- //nav -->
<!-- banner -->
	@if(Request::is('/')) @include('homepage.slider.slider') @endif
	
	
   
<!-- //banner -->
<!---728x90--->
<!-- banner-bottom -->
<div class="banner-bottom">
	<div class="row">
		@if( !! Request::is('/grafik')) @php $luas= '12';  @endphp @else @php $luas= '9'; @endphp @endif
		<div class="col-md-{{$luas}}">
			<div class="col-md-12">
			<div id="back">
					<div>		
						@yield("content")
					</div>
				</div>	
			</div>						
		</div>
		@if( Request::is('grafik/*')) @include('homepage.sidebar.sidebargrafik')  @else @include('homepage.sidebar.sidebar') @endif
	</div>
</div>

<hr>
	<div class="general">
		
<!-- Latest-tv-series -->
	
<!-- footer -->
@include('homepage.footer.footer')
	
<!-- //footer -->
<!-- Bootstrap Core JavaScript -->
 <script src="{{asset("homepage/js/jquery.slidey.js")}}"></script>
    <script src="{{asset("homepage/js/jquery.dotdotdot.min.js")}}"></script>

<script type="text/javascript">
    $('#sProvinsi').on('change', function(){
        $.post('{{url('drop') }}', {_token:'{{Session::token()}}', type: 'kabupaten_id', id: $('#sProvinsi').val()}, function(e){
            $('#sKabupaten').html(e);
        });

        $('#sKota').html('');
        $('#sKecamatan').html('');
        $('#sKelurahan').html('');
       
    });
    $('#sKabupaten').on('change', function(){
        $.post('{{ url('drop') }}',{_token:'{{Session::token()}}', type: 'kota_id', id: $('#sKabupaten').val()}, function(e){
            $('#sKota').html(e);
        });
        
        $('#sKecamatan').html('');
        $('#sKelurahan').html('');
    });
     $('#sKota').on('change', function(){
        $.post('{{ url('drop') }}',{_token:'{{Session::token()}}', type: 'kecamatan_id', id: $('#sKota').val()}, function(e){
            $('#sKecamatan').html(e);
        });
        
         
        $('#sKelurahan').html('');
    });
     $('#sKecamatan').on('change', function(){
        $.post('{{ url('drop') }}',{_token:'{{Session::token()}}', type: 'kelurahan_id', id: $('#sKecamatan').val()}, function(e){
            $('#sKelurahan').html(e);
        });
         
       
    });
     
</script>

    
	   <script type="text/javascript">
			$("#slidey").slidey({
				interval: 8000,
				listCount: 5,
				autoplay: false,
				showList: true
			});
			$(".slidey-list-description").dotdotdot();
		</script>
<script src="{{asset("homepage/js/bootstrap.min.js")}}"></script>
<script>

    $(function() {
      $('.adsense_btn_close').click(function() {
        $(this).closest('.adsense_fixed').css('display', 'none');
      });

      $('.adsense_btn_info').click(function() {
        if ($('.adsense_info_content').is(':visible')) {
          $('.adsense_info_content').css('display', 'none');
        } else {
          $('.adsense_info_content').css('display', 'block');
        }
      });

    });

  </script>
<script>
		$('.toggle').click(function(){
		  // Switches the Icon
		  $(this).children('i').toggleClass('fa-pencil');
		  // Switches the forms  
		  $('.form').animate({
			height: "toggle",
			'padding-top': 'toggle',
			'padding-bottom': 'toggle',
			opacity: "toggle"
		  }, "slow");
		});
	</script>
<script>

$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<!-- //Bootstrap Core JavaScript -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>

<!-- Mirrored from p.w3layouts.com/demos/aug-2016/24-08-2016/one_movies/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 11 Jun 2017 09:22:08 GMT -->
</html>