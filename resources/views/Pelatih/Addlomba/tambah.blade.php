@extends('template.pelatih.template-pelatih')
@section('content')
 

	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Tambah Peserta untuk {{$pertandingan->nama}} ({{$pertandingan->tahun->tahun}})</h5>
				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
		
			<div class="ibox-content">
				 

				{!! Form::open(['route'=>['pertandinganatlit.store',$pertandingan->id],'method'=>'post','class'=>'wizard-big','onsubmit'=>'return confirm("yakin dengan pilihan atlit anda?")']) !!}

					@include('Pelatih.Addlomba.form')

				{!! Form::close() !!}

			</div>
		</div>
	</div>



@stop
