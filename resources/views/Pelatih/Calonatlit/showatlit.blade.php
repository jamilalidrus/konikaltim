@extends('template.pelatih.template-pelatih')

@section("content")
	
	<div class="col-lg-12">
                <div class="contact-box">
                    @if(Session::has('message'))
                  <div class="alert alert-success alert-dismissable">
                       
                       <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>
                       
                  </div>  
                  @endif
                    <a href="#">
                    <div class="col-sm-4">
                        <div class="text-center">
                            <img alt="image"  class=" m-t-xs img-responsive img-responsive img-thumbnail" src="{{asset("gambar/user/$atlit->gambar")}}">
                            <div class="m-t-xs font-bold">Attlit</div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                    <div align="">

                          <h2 style="float: left;" ><i class="fa fa-user"></i> Profil Calon Atlit</h2>
                          @if($atlit->status == 'accepted' or $atlit->status == 'rejected') @else
                           <a style="float: right;" href="{{route('reject.atlit',$atlit->id)}}" confirm="yakin" class="btn btn-danger"><i class="fa fa-times"></i> Tolak &nbsp</a> &nbsp 
                          &nbsp&nbsp<a style="float: right;" href="{{route('accept.atlit',$atlit->id)}}" class="btn btn-primary"><i class="fa fa-check"></i> Terima</a>
                           @endif
                          
                    </div>
                  
                    <table class="table">
                    	<tr>
                    		<td><strong>Nama</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$atlit->name}}</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Umur</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{date("Y")-substr($atlit->tgl_lahir,0,4)}} Tahun</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Email</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$atlit->email}}</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Tanggal Lahir</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$atlit->tgl_lahir}}</strong></td>
                    	</tr>
                        <tr>
                            <td><strong>Jenis Kelamin</strong></td>
                            <td><strong>:</strong></td>
                            <td><strong>@if($atlit->kelamin ==1 )Laki-Laki @elseif($atlit->kelamin == 0) Perempuan @endif</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Ayah</strong></td>
                            <td><strong>:</strong></td>
                            <td><strong>{{$atlit->ayah}}</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Ibu</strong></td>
                            <td><strong>:</strong></td>
                            <td><strong>{{$atlit->ibu}}</strong></td>
                        </tr>
                    	<tr>
                    		<td><strong>Alamat</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>Provinsi {{$atlit->provinsi->nama_provinsi or 'N/A' }}  , Kabupaten {{$atlit->kabupaten->nama_kabupaten or 'N/A' }}  ,Kota {{$atlit->kota->nama_kota or 'N/A' }}  , Kecamatan {{$atlit->kecamatan->nama_kecamatan or 'N/A' }}  , Kelurahan {{$atlit->kelurahan->nama_kelurahan or 'N/A' }}  </strong></td>
                    	</tr>
                    	<tr>
                            <td><strong>Golongan Darah</strong></td>
                            <td><strong>:</strong></td>
                            <td><strong>{{$atlit->darah}}</strong></td>
                        </tr>
                    	<tr>
                    		<td><strong>Kontak Ponsel</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$atlit->no_hp}}</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Berat Badan</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$atlit->berat_badan}} kg</strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Mendaftar Sebagai</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$atlit->cabor->nama_cabor}} </strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Status</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong> <a class="btn btn-danger">{{$atlit->status}}</a> </strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Agama</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$atlit->agama}} </strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Status Kawin</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>@if($atlit->stutus=1) Sudah Kawin @else Belum Kawin @endif </strong></td>
                    	</tr>
                    	<tr>
                    		<td><strong>Terdata pada</strong></td>
                    		<td><strong>:</strong></td>
                    		<td><strong>{{$atlit->created_at->format('d M Y')}} </strong></td>
                    	</tr>
                        <tr>
                            <td><strong>Sertifikat</strong></td>
                            <td><strong>:</strong></td>
                            <td><strong>@if(empty($atlit->sertifikat->nama_sertifikat)) Empty Sertifikat @else <a class="btn btn-warning" href="{{route('download.atlit',$atlit->id)}}">Download</a> @endif</strong></td>
                        </tr>
                    </table>
                       
                       
                    </div>
                    <div class="clearfix"></div>
                        </a>
                </div>
            </div>
@stop()