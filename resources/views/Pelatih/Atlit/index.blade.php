@extends('template.pelatih.template-pelatih')
@section("content")
<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Semua Data Atlit</h5>

        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#">Config option 1</a>
            </li>
            <li><a href="#">Config option 2</a>
            </li>
          </ul>
          <a class="close-link">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
      
          @if(Session::has('message'))
          <div class="alert alert-success alert-dismissable">
            
            <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>
    
          </div>  
          @endif

        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover dataTables-example" >
            <thead>
            <?php  $no=1; ?>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Username</th>
                <th>email</th>
                <th>Tanggal Lahir</th>
                <th>Gambar</th>
                <th>Jenis Kelamin</th>
                <th>Pelatih</th>
                <th>Nama Ayah</th>
                <th>Nama Ibu</th>
                <th>No Hp</th>
                <th>Berat Badan</th>
                <th>Atlit</th>
                <th>Status Atlit</th>
                <th>Agama</th>
                <th>Status</th>
                <th>Tinggi Badan</th>
                <th>Darah</th>
                <th>Provinsi</th>
                <th>Kabupaten</th>
                <th>Kota</th>
                <th>Kecamatan</th>
                <th>Kelurahan</th>
                <th>Asal Sekolah</th>
                <th>Kelas</th>
                <th>Alamat Sekolah</th>
                <th>Ukuran Sepatu</th>
                <th>Ukuran Kaos</th>
                <th>Ukuran Kameja</th>
                <th>Hobi</th>             
              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              @foreach($atlit as $at)
              <tr class="gradeC">
                <td>{{$no++}}</td>
                <td>{{$at->name}}</td>
                <td>{{$at->username}}</td>              
                <td>{{$at->email}}</td>
                <td>{{$at->tgl_lahir}}</td>
                <td><a class="btn btn-primary" data-toggle="modal" data-target="#myModal{{$at->id}}"><i class="fa fa-image"></i> Show</a></td>
                <td> @if($at->kelamin = 1) Laki-Laki @else Perempuan  @endif</td>
                <td>{{$at->cabor->pelatih->first()->nama or 'N/A'}}</td>
                <td>{{$at->ayah}}</td>
                <td>{{$at->ibu}}</td>
                <td>{{$at->no_hp}}</td>
                <td>{{$at->berat_badan}}</td>
                <td>{{$at->cabor->nama_cabor}}</td>
                <td>{{$at->status_atlit}}</td>
                <td>{{$at->agama}}</td>
                <td>{{$at->status}}</td>
                <td>{{$at->tinggi}}</td>
                <td>{{$at->darah}}</td>
                <td>{{$at->provinsi->nama_provinsi or 'Belum Ada'}}</td>
                <td>{{$at->kabupaten->nama_kabupaten or 'Belum Ada'}}</td>
                <td>{{$at->kota->nama_kota or 'Belum Ada'}}</td>
                <td>{{$at->kecamatan->nama_kecamatan or 'Belum Ada'}}</td>
                <td>{{$at->kelurahan->nama_kelurahan or 'Belum Ada'}}</td>
                <td>{{$at->sekolah}}</td>
                <td>{{$at->kelas}}</td>
                <td>{{$at->alamat_sekolah}}</td>
                <td>{{$at->sepatu}}</td>
                <td>{{$at->kaos}}</td>
                <td>{{$at->kameja}}</td>
                <td>{{$at->hobi}}</td>                     
              </tr>
              <!-- modal gambar -->
              <div class="modal inmodal fade" id="myModal{{$at->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title">{{$at->nama}}</h4>
                          <small class="font-bold"></small>
                        </div>
                        <div class="modal-body">
                          <img class="img img-responsive img-thumbnail" src="{{asset("gambar/user/$at->gambar")}}">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
              <!-- endmodal gambar -->
              @endforeach()
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Username</th>
                <th>email</th>
                <th>Tanggal Lahir</th>
                <th>Gambar</th>
                <th>Jenis Kelamin</th>
                <th>Pelatih</th>
                <th>Nama Ayah</th>
                <th>Nama Ibu</th>
                <th>No Hp</th>
                <th>Berat Badan</th>
                <th>Atlit</th>
                <th>Status Atlit</th>
                <th>Agama</th>
                <th>Status</th>
                <th>Tinggi Badan</th>
                <th>Darah</th>
                <th>Provinsi</th>
                <th>Kabupaten</th>
                <th>Kota</th>
                <th>Kecamatan</th>
                <th>Kelurahan</th>
                <th>Asal Sekolah</th>
                <th>Kelas</th>
                <th>Alamat Sekolah</th>
                <th>Ukuran Sepatu</th>
                <th>Ukuran Kaos</th>
                <th>Ukuran Kameja</th>
                <th>Hobi</th>             
              </tr>
            </tfoot>
          </table>          
        </div>
        {!! $atlit->render() !!}
      </div>
    </div>
  </div>
</div>



      @stop()