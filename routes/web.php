<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
// 	return view('welcome');
// });

Auth::routes();

	// Route::get('/home', 'HomeController@index');
Route::get('login',function(){
	return view('login.praloginpage');
});
Route::get('/','Page\IndexController@index');
Route::get('news/{id}/{url}/read','Page\IndexController@show');
Route::group(['prefix'=>'home'],function(){
	Route::get('pendaftaran','Page\PendaftaranController@index')->name('pendaftaran');
	Route::post('submit/pendaftaran','Page\PendaftaranController@store')->name('pendaftaran.submit');
	Route::get('direct','Page\PendaftaranController@direct')->name('redirect');
});

// Route::get('/admin', 'AdminController@index');
Route::group(['prefix'=>'admin'],function(){
	Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::get('/adminlogout','Auth\AdminLoginController@logout')->name('admin.logout');
	Route::post('/proseslogin','Auth\AdminLoginController@login')->name('admin.login.submit');
	// Route::get('/','AdminController@index')->name('admin.dashboard');

	Route::resources([
		'homepage'=>'Admin\HomeController',
		'atlit'=>'Admin\UsersController',
		'admin'=>'Admin\AdminController',
		'cabor'=>'Admin\CaborController',
		'prestasi'=>'Admin\PrestasiController',
		'visimisi'=>'Admin\VisiMisiController',
		'sejarah'=>'Admin\SejarahController',
		'caborprestasi'=>'Admin\CaborPrestasiController',
		'berita'=>'Admin\BeritaController',
		'pelatih'=>'Admin\PelatihController',
		'tahun'=>'Admin\TahunController',
		'pertandingan'=>'Admin\PertandinganController',
		'listatlitlomba'=>'Admin\ListAtlitLombaController',
		'madin'=>'Admin\MadinController',
		'medali'=>'Admin\MedaliController',
		'regu'=>'Admin\ReguController',
		'reguprestasi'=>'Admin\ReguPrestasiController',
		'pengumumanadmin'=>'Admin\PengumumanController',
		
		]);
	Route::get('datatables','Admin\CaborController@anyData')->name('datatables.data');
	Route::get('atlitTable','Admin\UsersController@atlit')->name('atlit.data');
	
});

Route::group(['prefix'=>'atlit'],function(){
	Route::get('page-login','User\UserloginController@FormLogin')->name('user.login');
	Route::post('proses-login','User\UserloginController@proses')->name('user.proses.submit');
	Route::get('userlogout','User\UserloginController@logout')->name('user.out');

	Route::resources([
		'home'=>'User\HomeController',
		'profile'=>'User\ProfileSelfController',
		'cabangolah'=>'User\CaborController',
		'user'=>'User\AtlitController',
		'notif'=>'User\NotifController',
		'pengumuman'=>'User\PengumumanController',
		]);
});



Route::get('pelatih/login','Pelatih\PelatihLoginController@form')->name('pelatih.login');
Route::post('pelatih/proseslogin','Pelatih\PelatihLoginController@proses')->name('pelatih.proses');
Route::get('pelatih/proseslogout','Pelatih\PelatihLoginController@logout')->name('pelatih.logout');

Route::group(['prefix'=>'pelatih','middleware'=>'auth:pelatih'],function(){
	Route::get('atlitbinaan','Pelatih\AtlitController@mine');	
	Route::get('landing','Pelatih\HomeController@index');
	Route::get('allatlit','Pelatih\AtlitController@all');
	Route::get('calonatlit','Pelatih\AcceptAtlitController@index');
	Route::get('calonatlit/{id}','Pelatih\AcceptAtlitController@show')->name('select.atlit');
	Route::get('downloadfileatlit/{id}','Pelatih\AcceptAtlitController@download')->name('download.atlit');
	Route::get('accept/{id}/atlit','Pelatih\AcceptAtlitController@accept')->name('accept.atlit');
	Route::get('reject/{id}/atlit','Pelatih\AcceptAtlitController@reject')->name('reject.atlit');
	Route::get('selectatlit/{id}','Pelatih\SelectAtlitController@pertandingan');
	Route::get('atlitpertandingan','Pelatih\SelectAtlitController@index');
	Route::post('selectatlit/{id}/save','Pelatih\SelectAtlitController@store')->name('pertandinganatlit.store');
	Route::delete('delete/{id}/choice','Pelatih\SelectAtlitController@destroy')->name('choice.delete');

	Route::resources([
		'pengumuman'=>'Pelatih\PengumumanController',

		]);


});



Route::post('drop','DropdownController@dropdown');

Route::get('grafik/jumlahcatlit','Page\GrafikController@grafik')->name('cabor.user');
Route::get('grafik/umur','Page\GrafikController@byAge')->name('grafik.umur');
Route::get('grafik/medali','Page\GrafikController@bypiala')->name('grafik.piala');
Route::get('grafik/pelatih','Page\GrafikController@pelatih')->name('grafik.pelatih');

Route::get('visimisi/sejarah','Page\IndexController@visimisi')->name('koni.visimisi');
Route::get('allnews','Page\IndexController@allnews')->name('all.news');
Route::get('pengumuman','Page\IndexController@pengumuman')->name('all.pengumuman');
Route::get('pengumuman/{id}/{url}/read','Page\IndexController@readpengumuman')->name('pengumuman.read');
Route::get('bank','Page\IndexController@bank')->name('bank.data');

Route::get('t',function(){
	return App\User::first()->cabor;
	
});